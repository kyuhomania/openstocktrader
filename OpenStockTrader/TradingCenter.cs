﻿
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KiwoomCode;
using System.Globalization;
using System.Threading;

namespace OpenStockTrader
{
    public partial class TradingCenter : Form
    {
  
        public struct ConditionList
        {
            public string strConditionName;
            public int nIndex;
        }

        private int _scrNum = 5000;
        private string _strRealConScrNum = "0000";
        private string _strRealConName = "0000";
        private int _nIndex = 0;

        //private AxKHOpenAPILib.AxKHOpenAPI axKHOpenAPI;

        private KiwoomEventHandler eventHandler;
        private TraderContainer traderContainer;
        private Queue<Hashtable> eventQueue;
        private Queue<Hashtable> megaStockQueue;

        private MegaStock megaStock;

        // 화면번호 생산
        private string GetScrNum()
        {
            if (_scrNum < 5000 + 200)   // max 200 개로 한정
                _scrNum++;
            else
                _scrNum = 5000;

            return _scrNum.ToString();
        }

        // 실시간 연결 종료
        private void DisconnectAllRealData()
        {
            for( int i = _scrNum; i > 5000; i-- )
            {
                API.DisconnectRealData(i.ToString());
            }

            _scrNum = 5000;
        }

        public TradingCenter()
        {
            InitializeComponent();

            this.FormClosed += TradingCenter_FormClosed;

            eventQueue = new Queue<Hashtable>();
            megaStockQueue = new Queue<Hashtable>();
            eventHandler = new KiwoomEventHandler(this, axKHOpenAPI, eventQueue, megaStockQueue);

            axKHOpenAPI.OnReceiveTrData += new AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveTrDataEventHandler(eventHandler.OnReceiveTrData);
            axKHOpenAPI.OnReceiveRealData += new AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveRealDataEventHandler(eventHandler.OnReceiveRealData);
            axKHOpenAPI.OnReceiveMsg += new AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveMsgEventHandler(eventHandler.OnReceiveMsg);
            axKHOpenAPI.OnReceiveChejanData += new AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveChejanDataEventHandler(eventHandler.OnReceiveChejanData);
            axKHOpenAPI.OnEventConnect += new AxKHOpenAPILib._DKHOpenAPIEvents_OnEventConnectEventHandler(eventHandler.OnEventConnect);
            axKHOpenAPI.OnReceiveRealCondition += new AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveRealConditionEventHandler(eventHandler.OnReceiveRealCondition);
            axKHOpenAPI.OnReceiveTrCondition += new AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveTrConditionEventHandler(eventHandler.OnReceiveTrCondition);
            axKHOpenAPI.OnReceiveConditionVer += new AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveConditionVerEventHandler(eventHandler.OnReceiveConditionVer);

            axKHOpenAPI.CommConnect();
        }

        public void 서버연결이벤트()
        {
            API.SetAPIControl(axKHOpenAPI);

            lbl아이디.Text = API.GetLoginInfo("USER_ID");
            lbl이름.Text = API.GetLoginInfo("USER_NAME");
            string[] arr계좌 = API.GetLoginInfo("ACCNO").Split(';');
            lbl계좌번호.Text = arr계좌[0];

            traderContainer = new TraderContainer(lbl계좌번호.Text.Trim(), eventQueue, dgvTrader);

            string nameSpace = "OpenStockTrader";
            Assembly assembly = Assembly.GetExecutingAssembly();
            Type tAlgorithm = assembly.GetType(nameSpace + ".Algorithm");
            foreach (Type type in Assembly.GetExecutingAssembly().GetTypes())
            {
                if (type.Namespace == nameSpace && type.IsSubclassOf(tAlgorithm))
                {
                    cbo알고리즘.Items.Add(type.Name);
                }
            }

            // 조건 검색 불러오기
            //API.GetConditionLoad();

            megaStock = new MegaStock(lbl계좌번호.Text.Trim(), megaStockQueue);
            megaStock.Run();
            //Thread t = new Thread(megaStock.Run);
            //t.Start();
        }

        private void txtTrader종목명_TextChanged(object sender, EventArgs e)
        {
            lbl종목코드2.Text = API.Get종목코드(txtTrader종목명.Text.Trim());
        }

        private void txtTrader종목명_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && txtTrader종목명.Text.Trim() != "" && cbo알고리즘.SelectedText != "") Btn추가_Click(sender, e);
        }

        private void Btn추가_Click(object sender, EventArgs e)
        {
            string str종목명 = txtTrader종목명.Text.Trim();
            string str알고리즘 = (string)cbo알고리즘.SelectedItem;

            FileLogger.WriteLine("", "TradingCenter", "btn등록_Click",
                "종목명: " + str종목명 + " | 알고리즘:" + str알고리즘);

            if (str종목명 == "" || str알고리즘 == "") return;

            traderContainer.AddTrader(str종목명, API.Get종목코드(str종목명), str알고리즘);
        }

        private void Btn삭제_Click(object sender, EventArgs e)
        {
            traderContainer.RemoveTrader();
        }

        private void btn_조건일반조회_Click(object sender, EventArgs e)
        {
            API.SendCondition(GetScrNum(),
                                              cbo조건식.Text,
                                              cbo조건식.SelectedIndex,
                                              0);
        }

        private void btn조건실시간조회_Click(object sender, EventArgs e)
        {
            int lRet;
            string strScrNum = GetScrNum();
            lRet = API.SendCondition(strScrNum,
                                              cbo조건식.Text,
                                              cbo조건식.SelectedIndex,
                                              1);

            if (lRet == 1)
            {
                _strRealConScrNum = strScrNum;
                _strRealConName = cbo조건식.Text;
                _nIndex = cbo조건식.SelectedIndex;
            }
        }

        private void btn_조건실시간중지_Click(object sender, EventArgs e)
        {
            if (_strRealConScrNum != "0000" &&
                _strRealConName != "0000")
            {
                API.SendConditionStop(_strRealConScrNum, _strRealConName, _nIndex);
            }
        }

        private void TradingCenter_FormClosed(Object sender, FormClosedEventArgs e)
        {
            DisconnectAllRealData();
            traderContainer.stop();
            megaStock.stop();
        }
    }
}
