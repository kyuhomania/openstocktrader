﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace OpenStockTrader
{
    class FileLogger
    {
        public static void SetLogFile(string filename)
        {
            System.Diagnostics.Trace.Listeners.Add(new TextWriterTraceListener(filename));
            System.Diagnostics.Trace.AutoFlush = true;
        }

        public static void WriteLine(string eventID, string className, string methodName, string log)
        {
            string _eventID;
            if (eventID == "") _eventID = "00";
            else _eventID = eventID;
            System.Diagnostics.Trace.WriteLine("");
            System.Diagnostics.Trace.WriteLine("[" + DateTime.Now.ToString("HHmmss") + "][" + _eventID + "] "+ className + " - " + methodName);
            System.Diagnostics.Trace.WriteLine("           -  " + log);
        }

        public static void WriteLineEx(string eventID, string log)
        {
            string _eventID;
            if (eventID == "") _eventID = "00";
            else _eventID = eventID;
            System.Diagnostics.Trace.WriteLine("        [" + _eventID + "] " + log);
        }
    }
}
