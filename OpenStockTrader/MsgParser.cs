﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenStockTrader
{
    class MsgParser
    {
        private enum BuyStage { INITIAL, ITEM, PRICE, PRICETYPE, VOLUME };

        public static List<Order> Parse(string msg)
        {
            var orderList = new List<Order>();
            if (msg.Contains("모닝 브리핑"))
            {
                return orderList;
            }
            else if (msg.Contains("매도하세요")
                || msg.Contains("매도합니다")
                || msg.Contains("수익실현")
                //|| msg.Contains("비중축소") 
                )
            {
                var subMsg = msg;
                var order = new Order();
                order.type = Order.OrderType.SELL;
                subMsg = subMsg.Replace("[Web발신]","");
                subMsg = subMsg.Substring(subMsg.IndexOf("]") + 1);
                var splitMsg = subMsg.Split(null);
                //splitMsg.ToList().ForEach(i => Console.WriteLine(i.ToString()));

                if (splitMsg[0].EndsWith("보유자"))
                {
                    splitMsg[0] = splitMsg[0].Substring(0, splitMsg[0].Length-3).Trim();
                }

                order.item = splitMsg[0].ToUpper();


                foreach (string s in splitMsg)
                {
                    var ss = s.Trim();

                    if (ss.Contains("원부근에서"))
                    {
                        order.price = Convert.ToInt32(ss.Replace("원부근에서", ""));
                    }
                    else if (ss.Contains("원부근"))
                    {
                        order.price = Convert.ToInt32(ss.Replace("원부근", ""));
                    }
                    else if (ss.Contains("원"))
                    {
                        order.price = Convert.ToInt32(ss.Substring(0,ss.IndexOf("원")));
                    }

                    if (subMsg.Contains("1/2") || subMsg.Contains("절반"))
                    {
                        order.volume = 0.5f;
                    }

                    if (subMsg.Contains("1/3"))
                    {
                        order.volume = 0.33f;
                    }
                }
                if (order.item.Contains(","))
                {
                    var splitItems = order.item.Split(',');
                    foreach (string item in splitItems) {
                                Order order2 = new Order();
                        order2.type = Order.OrderType.SELL;
                        order2.item = item.Trim();
                        order2.volume = order.volume;
                        order2.price = order.price;
                        orderList.Add(order2);
                        FileLogger.WriteLine("", "MsgParser", "parse", order2.ToString());
                        Console.WriteLine(order2);
                    }

                }
                else
                {
                    orderList.Add(order);
                    FileLogger.WriteLine("", "MsgParser", "parse", order.ToString());
                    Console.WriteLine(order);
                }
                
                return orderList;
            }
            else if (msg.Contains("☞"))
            {
                var subMsg = msg;
                while (true)
                {
                    var order = new Order();
                    order.type = Order.OrderType.BUY;
                    var stage = BuyStage.INITIAL;
                    subMsg = subMsg.Substring(subMsg.IndexOf("☞")+1);
                    //Console.WriteLine("subMst:"+subMsg);
                    var splitMsg = subMsg.Split(null);
                    //splitMsg.ToList().ForEach(i => Console.WriteLine(i.ToString()));

                    foreach (string s in splitMsg)
                    {
                        var ss = s.Trim();
                        if (ss.Length==0 || ss.StartsWith("(") )
                        {
                            continue;
                        }
                        //Console.WriteLine("stage:"+stage+" msg:"+ss);
                    
                        if (stage == BuyStage.INITIAL)
                        {
                            order.item = ss.ToUpper();
                            if (order.item.Contains("("))
                            {
                                order.item = order.item.Substring(0, order.item.IndexOf("(")).Trim();
                            }
                            stage = BuyStage.ITEM;
                        }
                        else if (stage == BuyStage.ITEM)
                        {
                            order.price = Convert.ToInt32(ss.Replace("원", ""));
                            stage = BuyStage.PRICE;
                        }
                        else if (stage == BuyStage.PRICE)
                        {
                            if (ss.Equals("부근"))
                            {
                                order.priceType = Order.OrderPriceType.AROUND;
                            }
                            else if (ss.Equals("이하"))
                            {
                                order.priceType = Order.OrderPriceType.BELOW;
                            }
                            else
                            {
                                order.priceType = Order.OrderPriceType.UNKNOWN;
                                
                            }
                            stage = BuyStage.PRICETYPE;
                        }
                        else if (stage == BuyStage.PRICETYPE)
                        {
                            if (ss.EndsWith("%"))
                            {
                                order.volume = Convert.ToInt32(ss.Replace("%", "")) / 100f;
                            }
                            stage = BuyStage.VOLUME;
                            break;
                        }
                    }

                    if (stage == BuyStage.VOLUME)
                    {
                        FileLogger.WriteLine("", "MsgParser", "parse", order.ToString());
                        Console.WriteLine(order);
                        orderList.Add(order);
                    }

                    if (subMsg.Contains("☞")==false)
                    {
                        break;
                    }
                }
                return orderList;
            }
            else
            {
                FileLogger.WriteLine("", "MsgParser", "parse", "Parse error msg");
                Console.WriteLine("Parse error msg: " + msg);
            }
            return orderList;
        }

        public static readonly string[] buyMsgList = {
            "[Web발신]\n메가스탁]\n\n\n※ 메가스탁 프리미엄\n\n[오늘의 추천공략주]\n\n☞ 현대약품\n\n5640원 부근 10% 분할매수",
            "[Web발신]\n메가스탁]\n\n\n※ 메가스탁 프리미엄\n\n[오늘의 추천공략주]\n\n☞ 영인프런티어\n\n5550원 부근 5% 분할매수 합니다.\n\n☞ 풍국주정\n\n15400원 부근 10% 분할매수 합니다\n\n\n미보유자 분들은 매수합니다.",
            "[Web발신]\n메가스탁]\n※ 메가스탁 프리미엄\n\n[오늘의 추천공략주]\n\n☞ 엠케이전자\n\n11900원 부근 10% 분할매수",
            "[Web발신]\n메가스탁]\n\n\n※ 메가스탁 프리미엄\n\n[오늘의 추천공략주]\n\n☞ 펄어비스\n\n257000원 부근 10% 분할",
            "[Web발신]\n메가스탁]\n\n\n※ 메가스탁 프리미엄\n\n[오늘의 추천공략주]\n\n☞ 메지온\n\n44000원 이하 10% 분할매수 합니다.\n\n☞ 한미반도체\n\n12000원 이하 10% 분할매수 합니다.\n\n\n미보유자 분들은 매수합니다.",
            "[Web발신]\n메가스탁]\n\n\n※ 메가스탁 프리미엄\n\n[오늘의 추천공략주]\n\n☞ 한컴시큐어(단기)\n\n6100원 부근 10% 분할매수 합니다. , 손절 -3%\n\n미보유자 분들은 매수합니다.",
            "[Web발신]\n메가스탁]\n\n※ 메가스탁 프리미엄\n\n[오늘의 추천공략주]\n\n☞ 대봉엘에스(단기)\n\n13800원 이하 5%",
            "[Web발신]\n메가스탁]\n※ 메가스탁 프리미엄\n\n[오늘의 추천공략주]\n\n☞ 씨티엘 (단기)\n\n4480원 부근 10% 분",
        };

        public static readonly string[] sellMsgList =
        {
            "[Web발신]\n메가스탁]롯데하이마트 보유자 1/3 매도하세요.",
            "[Web발신]\n메가스탁]바디텍메드 절반 수익실현하세요.",
            "[Web발신]\n메가스탁]성창오토텍보유자 절반 매도하세요.",
            "[Web발신]\n메가스탁]한국토지신탁 보유자 매수가근처 매도하세요.",
            "메가스탁]SK증권 보유자 절반 매도하세요.",
            "[Web발신]\n메가스탁]종근당홀딩스 보유자 6350원부근에서 매도하세요.",
            "[Web발신]\n메가스탁]에이디테크놀로지 보유자 11850원 부근 1/3매도하세요.",
            "[Web발신]\n메가스탁]셀루메드 보유자 절반매도하세요.",
            "메가스탁]풍국주정 보유자 1/3 매도하세요.",
            "[Web발신]\n메가스탁]풍국주정 보유자 매도하세요.",
            "메가스탁]성창오토텍 보유자 자율매도하세요.",
            "[Web발신]\n메가스탁]유안타증권 보유자 1/3 매도하세요.",
            "[Web발신]\n메가스탁]티웨이홀딩스 보유자 4350원 부근 1/3매도하세요.",
            "[Web발신]\n메가스탁]펄어비스 보유자 비중축소하세요.",
            "[Web발신]\n메가스탁]바디텍메드,메지온 보유자 매도하세요.",
            "[Web발신]\n메가스탁]종근당홀딩스 보유자 절반매도하세요.\n\n장 후반 지수 약세될 수 있으니 비중 큰 종목들은 자율적으로",
            "[Web발신]\n메가스탁]GS, 한화 보유자 비중축소하세요.",
            "[Web발신]\n메가스탁]한컴시큐어 보유자 6350원부근에서 매도하세요.",
            "[Web발신]\n메가스탁]제주반도체 보유자 6050원 부근 1/3매도하세요.",
        };
    }
}
