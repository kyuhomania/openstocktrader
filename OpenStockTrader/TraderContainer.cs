﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Timers;
using System.Text;
using System.Threading.Tasks;

namespace OpenStockTrader
{

    public class TraderContainer
    {
        string str계좌번호 = "";
        Queue<Hashtable> eventQueue;
        ArrayList alstOwnedStock;
        DataGridView dgvTrader;
        ArrayList alTrader;

        Boolean isRunning;

        private struct OwnedStock
        {
            private string _str종목명;
            private string _str종목코드;
            private string _str현재가;
            private string _str보유수량;
            private string _str평균단가;

            public string str종목명 { get { return _str종목명; } set { _str종목명 = value; } }
            public string str종목코드 { get { return _str종목코드; } set { _str종목코드 = value; } }
            public string str현재가 { get { return _str현재가; } set { _str현재가 = value; } }
            public string str보유수량 { get { return _str보유수량; } set { _str보유수량 = value; } }
            public string str평균단가 { get { return _str평균단가; } set { _str평균단가 = value; } }
        }

        public TraderContainer(string str계좌번호, Queue<Hashtable> eventQueue, DataGridView dgvTrader)
        {
            this.str계좌번호 = str계좌번호;
            this.eventQueue = eventQueue;
            this.dgvTrader = dgvTrader;
            isRunning = true;

            alstOwnedStock = new ArrayList();
            Request보유주식정보();

            alTrader = new ArrayList();
            AddTraderFromFile();

            Thread t = new Thread(new ThreadStart(Run));
            t.Start();
        }

        void Run()
        {
            while (isRunning)
            {
                if (eventQueue.Count() == 0)
                {
                    Thread.Sleep(100);
                    continue;
                }

                Hashtable eventItem = eventQueue.Dequeue();
                string eventID = (string)eventItem["eventID"];
                string str이벤트 = (string)eventItem["이벤트"];

                FileLogger.WriteLine(eventID, "TraderContainer", "Run",
                        "이벤트:" + str이벤트);

                switch (str이벤트)
                {
                    case "OnReceiveRealData":
                        if ((string)eventItem["실시간타입"] == "주식체결")
                        {
                            string str종목코드 = (string)eventItem["종목코드"];
                            string str현재가 = (string)eventItem["현재가"];

                            UpdateOwnedStock(eventID, API.Get종목명(str종목코드), str종목코드, str현재가, "", "");
                            UpdateDgvTraderRows(eventID, API.Get종목명(str종목코드), str종목코드, str현재가, "", "", "", "");
                        }
                        OnReceiveRealData(eventItem);
                        break;

                    case "OnReceiveTrData":
                        if ((string)eventItem["요청이름"] == "보유주식정보")
                        {
                            string str종목명 = (string)eventItem["종목명"];
                            string str종목코드 = (string)eventItem["종목코드"];
                            string str현재가 = (string)eventItem["현재가"];
                            string str보유수량 = (string)eventItem["보유수량"];
                            string str평균단가 = (string)eventItem["평균단가"];
                            string str손익금액 = (string)eventItem["손익금액"];
                            string str손익율 = (string)eventItem["손익율"];

                            UpdateOwnedStock(eventID, str종목명, str종목코드, str현재가, str보유수량, str평균단가);
                            UpdateDgvTraderRows(eventID, str종목명, str종목코드, str현재가, str보유수량, str평균단가, str손익금액, str손익율);
                        }
                        break;

                    case "OnReceiveMsg":
                        break;

                    case "OnReceiveChejanData":
                        OnReceiveChejanData(eventItem);
                        break;
                }
            }
        }

        public void OnReceiveRealData(Hashtable eventItem)
        {
            string eventID = (string)eventItem["eventID"];
            string str종목명 = (string)eventItem["종목명"];
            FileLogger.WriteLine(eventID, "TraderContainer", "OnReceiveRealData",
                "종목명: " + str종목명);

            foreach (DataGridViewRow row in dgvTrader.Rows)
            {
                string str종목코드 = (string)row.Cells[1].Value;
                if (str종목코드 == API.Get종목코드(str종목명))
                {
                    Trader trader = (Trader)alTrader[row.Index];
                    trader.OnReceiveRealData(eventItem);
                }
            }
        }

        public void OnReceiveChejanData(Hashtable eventItem)
        {
            string eventID = (string)eventItem["eventID"];
            string str종목명 = (string)eventItem["종목명"];
            string str체결구분 = (string)eventItem["체결구분"];
            string str체결수량 = (string)eventItem["체결수량"];
            string str체결가격 = (string)eventItem["체결가격"];

            FileLogger.WriteLine(eventID, "TraderContainer", "OnReceiveChejanData",
                "종목명: " + str종목명 + " | 체결구분:" + str체결구분 );
            
            if(str체결구분 == "주문체결통보" && str체결수량 != "")
            {
                FileLogger.WriteLineEx(eventID, "체결수량: " + str체결수량 + " | 체결가격:" + str체결가격);

                foreach (DataGridViewRow row in dgvTrader.Rows)
                {
                    string str종목코드 = (string)row.Cells[1].Value;
                    if (str종목코드 == API.Get종목코드(str종목명))
                    {
                        Trader trader = (Trader)alTrader[row.Index];
                        trader.OnReceiveChejanData(eventItem);
                    }
                }
            }
        }

        public void AddTrader(string str종목명, string str종목코드, string str알고리즘)
        {
            FileLogger.WriteLine("", "TraderContainer", "AddTrader",
                 "종목명: " + str종목명 + " | 종목코드:" + str종목코드 + " | 알고리즘:" + str알고리즘);

            FileStream fs = new FileStream("StockStats.db", FileMode.Open, FileAccess.Write);
            StreamWriter st = new StreamWriter(fs, Encoding.UTF8);

            st.BaseStream.Seek(0, SeekOrigin.End);
            st.WriteLine(str종목명 + "|" + str종목코드 + "|" + str알고리즘);

            st.Close();
            fs.Close();

            AddTraderToDgvTraderAndAlTrader(str종목명, str종목코드, str알고리즘);
        }

        private string _bRealData = "0";   // 0: 최초 또는 overwrite, 1: 추가
        private void AddTraderToDgvTraderAndAlTrader(string str종목명, string str종목코드, string str알고리즘)
        {
            FileLogger.WriteLine("", "TraderContainer", "AddTraderToDgvTraderAndAlTrader",
                "종목명: " + str종목명 + " | 종목코드:" + str종목코드 + " | 알고리즘:" + str알고리즘);

            string nameSpace = "OpenStockTrader";
            Assembly assembly = Assembly.GetExecutingAssembly();
            Type t = assembly.GetType(nameSpace + "." + str알고리즘);
            Algorithm algorithm = (Algorithm)Activator.CreateInstance(t);

            int lRet = API.SetRealReg("1111", str종목코드, "9001;10", _bRealData);
            if (lRet == 0)  _bRealData = "1";   // 최초 등록 후 부터는 종목 추가

            Trader trader = new Trader(str계좌번호, str종목명, str종목코드, algorithm);
            trader.algorithm.str종목명 = str종목명;
            trader.algorithm.str종목코드 = str종목코드;
            int index = dgvTrader.Rows.Add(str종목명, str종목코드, str알고리즘, 0, 0, 0, 0, 0);
            alTrader.Insert(index, trader);

            foreach (OwnedStock ownedStock in alstOwnedStock)
            {
                if (ownedStock.str종목명 == str종목명)
                {
                    FileLogger.WriteLineEx("", "OwnedStock 목록 내의 종목 정보 | 종목명: " + ownedStock.str종목명 + 
                        " | 종목코드: " + ownedStock.str종목명 + 
                        " | 현재가: " + ownedStock.str현재가 +
                        " | 보유수량: " + ownedStock.str보유수량 +
                        " | 평균단가: " + ownedStock.str평균단가);

                    trader.algorithm.str현재가 = ownedStock.str현재가;
                    trader.algorithm.str보유수량 = ownedStock.str보유수량;
                    trader.algorithm.str평균단가 = ownedStock.str평균단가;

                    int i현재가 = int.Parse(ownedStock.str현재가);
                    int i보유수량 = int.Parse(ownedStock.str보유수량);
                    int i평균단가 = int.Parse(ownedStock.str평균단가);

                    dgvTrader.Rows[index].Cells[3].Value = i현재가;
                    dgvTrader.Rows[index].Cells[4].Value = i보유수량;
                    dgvTrader.Rows[index].Cells[5].Value = i평균단가;

                    int i손익금액 = (i현재가 - i평균단가) * i보유수량;
                    dgvTrader.Rows[index].Cells[6].Value = i손익금액;
                    dgvTrader.Rows[index].Cells[7].Value = ((float)i손익금액 / (float)(i평균단가 * i보유수량)) * 100F;
                }
            }
        }

        private void AddTraderFromFile()
        {
            FileStream fs = new FileStream("StockStats.db", FileMode.OpenOrCreate, FileAccess.Read);
            StreamReader st = new StreamReader(fs, Encoding.UTF8);

            st.BaseStream.Seek(0, SeekOrigin.Begin);

            while (st.Peek() > -1)
            {
                string[] item = st.ReadLine().Split('|');
                AddTraderToDgvTraderAndAlTrader(item[0].Trim(), item[1].Trim(), item[2].Trim());
            }

            st.Close();
            fs.Close();
        }

        public void RemoveTrader()
        {
            FileLogger.WriteLine("", "TraderContainer", "RemoveTrader", "");

            RemoveTraderFormAddTraderToDgvTraderAndAlTrader();

            FileStream fs = new FileStream("StockStats.db", FileMode.Create, FileAccess.Write);
            StreamWriter st = new StreamWriter(fs, Encoding.UTF8);

            foreach (DataGridViewRow item in dgvTrader.Rows)
            {
                string str종목명 = (string)item.Cells[0].Value;
                string str종목코드 = (string)item.Cells[1].Value;
                string str알고리즘 = (string)item.Cells[2].Value;

                FileLogger.WriteLineEx("", "StockStats.db Write : " + str종목명 + "|" + str종목코드 + "|" + str알고리즘);

                st.BaseStream.Seek(0, SeekOrigin.End);
                st.WriteLine(str종목명 + "|" + str종목코드 + "|" + str알고리즘);
            }

            st.Close();
            fs.Close();
        }

        private void RemoveTraderFormAddTraderToDgvTraderAndAlTrader()
        {
            FileLogger.WriteLine("", "TraderContainer", "RemoveTraderFormAddTraderToDgvTraderAndAlTrader", "");

            foreach (DataGridViewRow item in dgvTrader.SelectedRows)
            {
                FileLogger.WriteLineEx("", "ItemIndex:" + item.Index);
                alTrader.RemoveAt(item.Index);
                dgvTrader.Rows.RemoveAt(item.Index);
            }
        }

        private void Request보유주식정보()
        {
            FileLogger.WriteLine("", "TraderContainer", "Request보유주식정보",
                "계좌번호:" + str계좌번호);
            if (str계좌번호 == "") return;
            API.SetInputValue("계좌번호", str계좌번호);
            API.SetInputValue("비밀번호", "");
            API.SetInputValue("상장폐지조회구분", "1");
            API.SetInputValue("비밀번호입력매체구분", "00");
            API.CommRqData("보유주식정보", "OPW00004", 0, "0002");
        }

        private void UpdateOwnedStock(string eventID, string str종목명, string str종목코드, string str현재가, string str보유수량, string str평균단가)
        {
            FileLogger.WriteLine(eventID, "TraderContainer", "UpdateOwnedStock",
                "종목명: " + str종목명 + " | 종목코드:" + str종목코드 + " | 현재가:" + str현재가 + " | 보유수량:" + str보유수량 + " | 평균단가:" + str평균단가);

            int aIndex = 0;
            for (aIndex = 0; aIndex < alstOwnedStock.Count; aIndex++)    // 목록에 있으면 값 변경
            {
                OwnedStock stock = (OwnedStock)alstOwnedStock[aIndex];
                //FileLogger.WriteLineEx(eventID, "OwnedStock[" + aIndex + "] " + stock.str종목명);

                if (stock.str종목코드 == str종목코드)
                {
                    if(str현재가 != "") stock.str현재가 = str현재가;
                    if (str보유수량 != "") stock.str보유수량 = str보유수량;
                    if (str평균단가 != "") stock.str평균단가 = str평균단가;
                    break;
                }
            }

            if(aIndex == alstOwnedStock.Count && str보유수량 != "")   // 보유한 종목이나 목록에 없으면 신규 추가
            {
                FileLogger.WriteLineEx(eventID, "OwnedStock[" + aIndex + "] " +  str종목명 + " <= 신규 추가");
                OwnedStock ownedStock = new OwnedStock();
                ownedStock.str종목명 = str종목명;
                ownedStock.str종목코드 = str종목코드;
                ownedStock.str현재가 = str현재가;
                ownedStock.str평균단가 = str평균단가;
                ownedStock.str보유수량 = str보유수량;
                alstOwnedStock.Add(ownedStock);
            }
        }

        private void UpdateDgvTraderRows(string eventID, string str종목명, string str종목코드, string str현재가, string str보유수량, string str평균단가, string str손익금액, string str손익율)
        {
            FileLogger.WriteLine(eventID, "TraderContainer", "UpdateDgvTraderRows",
                "종목명: " + str종목명 + " | 종목코드:" + str종목코드 + " | 현재가:" + str현재가 + " | 보유수량:" + str보유수량 + " | 평균단가:" + str평균단가 + " | 손익금액:" + str손익금액 + " | 손익율:" + str손익율);

            foreach (DataGridViewRow rows in dgvTrader.Rows)
            {
                string 종목명 = (string)rows.Cells[0].Value;
                string 종목코드 = (string)rows.Cells[1].Value;

                //FileLogger.WriteLineEx(eventID, "종목명 : " + 종목명 + " | 종목코드:" + 종목코드);

                if (종목코드 == str종목코드)
                {
                    FileLogger.WriteLineEx(eventID, "Update " + 종목명 + " dgvTrader.Rows");
                    int i현재가 = Math.Abs(int.Parse(str현재가));
                    if ((int)rows.Cells[3].Value != i현재가)
                    {
                        rows.Cells[3].Value = i현재가;
                        // 변경된 가격 정보만 입력
                        if (str보유수량 == "")
                        {
                            int i보유수량 = (int)rows.Cells[4].Value;
                            int i평균단가 = (int)rows.Cells[5].Value;
                            int i손익금액 = (i현재가 - i평균단가) * i보유수량;
                            rows.Cells[6].Value = i손익금액;
                            rows.Cells[7].Value = ((float)i손익금액 / (float)(i평균단가 * i보유수량)) * 100F;
                        }
                        else
                        {
                            rows.Cells[4].Value = int.Parse(str보유수량);
                            rows.Cells[5].Value = int.Parse(str평균단가);
                            rows.Cells[6].Value = int.Parse(str손익금액);
                            rows.Cells[7].Value = float.Parse(str손익율) / 10000F;
                        }
                    }
                }
            }
        }

        public void stop()
        {
            isRunning = false;
        }
    }
}
