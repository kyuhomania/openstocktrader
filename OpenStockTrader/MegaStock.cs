﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using System.Web;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Collections;
using Grapevine.Server;
using Grapevine.Server.Attributes;
using Grapevine.Shared;
using Grapevine.Interfaces.Server;

namespace OpenStockTrader
{
    class MegaStock
    {
        // Configuration
        int Port = 12000;
        int 종목당최대거래비용 = 200000;

        private String account = "";
        Queue<Hashtable> megaStockQueue;
        Hashtable WaitingItem;
        private OrderDupChecker orderDupChecker;
        Boolean isRunning;

        //RestServer restServer;
        Socket megaSocket;

        public MegaStock(String account, Queue<Hashtable> megaStockQueue)
        {
            this.account = account;
            this.megaStockQueue = megaStockQueue;
            WaitingItem = new Hashtable();
            orderDupChecker = new OrderDupChecker();
            isRunning = true;
        }

        public void Run()
        {
            Thread receiveMegaOrderThread = new Thread(new ThreadStart(ReceiveForwarderMessage));
            receiveMegaOrderThread.Start();
            Thread receiveKiwoomEventThread = new Thread(new ThreadStart(ReceiveKiwoomEvent));
            receiveKiwoomEventThread.Start();

            /*
            // netsh http add urlacl url=http://+:9999/ user=Everyone
            ServerSettings settings = new ServerSettings();
            var server = new RestServer(settings);
            server.Host = "+";
            server.Port = Port.ToString();
            server.Router.Register(new Route(typeof(ServerRestResource).GetMethod("HandleMsg")));
            try
            {
                server.LogToConsole().Start();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.ToString());
            }
            */
        }

        private class ServerRestResource
        {
            public ServerRestResource()
            {
                ;
            }

            [RestRoute(HttpMethod = HttpMethod.POST, PathInfo = "/msg")]
            public IHttpContext HandleMsg(IHttpContext context)
            {
                var msg = WebUtility.UrlDecode(context.Request.Payload);
                Console.WriteLine(msg);
                List<Order> orderList = MsgParser.Parse(msg);
                context.Response.SendResponse("ok");
                return context;
            }
        }

        void ReceiveForwarderMessage()
        {
            IPEndPoint ipep = new IPEndPoint(IPAddress.Any, Port);
            megaSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            megaSocket.Bind(ipep);
            megaSocket.Listen(20);
            while (isRunning)
            {
                Socket client = null;
                try
                {
                    client = megaSocket.Accept();
                    FileLogger.WriteLine("", "MegaStock", "ReceiveForwarderMessage", "메가스탁 메시지 수신");
                    String content = ParseMessage(client);
                    client.Send(Header(client, content));
                }
                catch (Exception e)
                {
                    FileLogger.WriteLine("", "MegaStock", "ReceiveForwarderMessage", "메시지 수신 에러 exception: " + e.Message);
                }
                finally
                {
                    if (client != null)
                    {
                        client.Close();
                    }
                }
            }
        }

        public String ParseMessage(Socket client)
        {
            byte[] _data = new byte[4096];

            client.Receive(_data);
            String _data_str = WebUtility.UrlDecode(Encoding.Default.GetString(_data).Trim('\0'));

            String[] separator = new String[1] {"\r\n\r\n"};
            String[] data = _data_str.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            String header = data[0];
            String body = data[1];
            if (header.StartsWith("POST /msg"))
            {
                FileLogger.WriteLine("", "MegaStock", "ParseMessage", "BODY : " + body);

                List<Order> orderList = MsgParser.Parse(body);

                foreach (Order order in orderList)
                {
                    FileLogger.WriteLine("", "MegaStock", "ParseMessage", "ORDER : " + order.ToString());

                    if (orderDupChecker.isDuplicate(order) == true)
                    {
                        FileLogger.WriteLineEx("", "Duplicated Order");
                        continue;
                    }

                    String itemCode = API.Get종목코드(order.item);
                    if (itemCode == null)
                    {
                        FileLogger.WriteLineEx("", order.item + "의 종목코드가 없습니다.");
                        continue;
                    }

                    Hashtable item = new Hashtable();
                    item.Add("item", order.item);
                    item.Add("type", order.type);
                    item.Add("itemCode", itemCode);
                    item.Add("price", order.price);

                    WaitingItem.Add(order.item, item);

                    switch (order.type)
                    {
                        case Order.OrderType.BUY:
                            {
                                Request종목정보(itemCode);
                            }
                            break;

                        case Order.OrderType.SELL:
                            {
                                Request보유종목정보();
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
            return "ok";
        }

        void ReceiveKiwoomEvent()
        {
            while (isRunning)
            {
                if (megaStockQueue.Count() == 0)
                {
                    Thread.Sleep(100);
                    continue;
                }

                Hashtable eventItem = megaStockQueue.Dequeue();

                string eventID = (string)eventItem["eventID"];
                string 종목명 = (string)eventItem["종목명"];
                string TrCode = (string)eventItem["TrCode"];

                FileLogger.WriteLine(eventID, "MegaStock", "ReceiveKiwoomEvent", "종목명:" + 종목명 + " | TrCode:" + TrCode);

                Hashtable item = (Hashtable)WaitingItem[종목명];
                WaitingItem.Remove(종목명);
                if (item != null) {

                    int type = (int)item["type"];
                    String itemCode = (String)item["itemCode"];
                    int price = (int)item["price"];

                    FileLogger.WriteLine(eventID, "MegaStock", "ReceiveKiwoomEvent", "종목명:" + 종목명 + " | itemCode: " + itemCode + " | type: " + type + " | price: " + price);

                    if(TrCode.Equals("OPT10001") && type == (int)Order.OrderType.BUY)
                    {
                        int 현재가 = Math.Abs(int.Parse((string)eventItem["현재가"]));
                        if ((float)Math.Abs(현재가 - price) < ((float)현재가 * 0.05f))
                        {
                            int 주문수량 = 종목당최대거래비용 / price;

                            FileLogger.WriteLine(eventID, "MegaStock", "ReceiveKiwoomEvent", "매수 주문수량 : " + 주문수량 + " | 매수가격: " + price);
                            int ret = API.SendOrder("매수주문", "99900", account, 1/*매수*/, itemCode, 주문수량, price, "00", "");
                            FileLogger.WriteLine(eventID, "MegaStock", "ReceiveKiwoomEvent", "매수 매수주문결과 : " + ret);
                        }
                        else
                        {
                            FileLogger.WriteLine(eventID, "MegaStock", "ReceiveKiwoomEvent", "매수 주식의 현재가와 매수가 불일치");
                        }


                    }
                    else if(TrCode.Equals("OPW00004") && type == (int)Order.OrderType.SELL)
                    {
                        string 보유수량 = (string)eventItem["보유수량"];
                        string sHogaGb = "00";
                        if (price == 0)
                        {
                            sHogaGb = "03";
                        }

                        FileLogger.WriteLine(eventID, "MegaStock", "ReceiveKiwoomEvent", "매도 주문수량 : " + 보유수량 + " | 호가주문");
                        int ret = API.SendOrder("매도주문", "99901", account, 2/*매도*/, itemCode, int.Parse(보유수량), price, sHogaGb, "");
                        FileLogger.WriteLine(eventID, "MegaStock", "ReceiveKiwoomEvent", "매도 주문결과 : " + ret);
                    }
                }
            }
        }

        public byte[] Header(Socket client, String content)
        {
            byte[] _data2 = Encoding.Default.GetBytes(content);
            try
            {

                String _buf = "HTTP/1.0 200 ok\r\n";
                _buf += "Data: " + " " + "\r\n";
                _buf += "Content-Length: " + content.Length.ToString() + "\r\n";
                _buf += "content-type:text/html\r\n";
                _buf += "\r\n";
                client.Send(Encoding.Default.GetBytes(_buf));
            }
            catch
            {
                String _buf = "HTTP/1.0 100 BedRequest ok\r\n";
                _buf += "content-type:text/html\r\n";
                _buf += "\r\n";
                client.Send(Encoding.Default.GetBytes(_buf));
                _data2 = Encoding.Default.GetBytes("Bed Request");
            }
            return _data2;
        }

        private void Request종목정보(String itemCode)
        {
            FileLogger.WriteLine("", "MegaStock", "Request종목정보", "종목코드 = " + itemCode);
            API.SetInputValue("종목코드", itemCode);
            int ret = API.CommRqData("MegaStock", "OPT10001", 0, "0203");
            FileLogger.WriteLineEx("", "Request종목정보 ret = " + ret);
        }

        private void Request보유종목정보()
        {
            FileLogger.WriteLine("", "MegaStock", "Request종목정보", "");
            API.SetInputValue("계좌번호", account);
            API.SetInputValue("비밀번호", "");
            API.SetInputValue("상장폐지조회구분", "1");
            API.SetInputValue("비밀번호입력매체구분", "00");
            int ret = API.CommRqData("MegaStock", "OPW00004", 0, "0103");
            FileLogger.WriteLineEx("", "Request보유종목정보 ret = " + ret);
        }

        public void stop()
        {
            isRunning = false;
            megaSocket.Close();
            //restServer.Stop();
        }
    }
}
