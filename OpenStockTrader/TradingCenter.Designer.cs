﻿namespace OpenStockTrader
{
    partial class TradingCenter
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TradingCenter));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.axKHOpenAPI = new AxKHOpenAPILib.AxKHOpenAPI();
            this.Label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl이름 = new System.Windows.Forms.Label();
            this.lbl아이디 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbo조건식 = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn_조건실시간중지 = new System.Windows.Forms.Button();
            this.btn조건실시간조회 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.btn_조건일반조회 = new System.Windows.Forms.Button();
            this.groupTrader등록해제 = new System.Windows.Forms.GroupBox();
            this.dgvTrader = new System.Windows.Forms.DataGridView();
            this.cbo알고리즘 = new System.Windows.Forms.ComboBox();
            this.lbl알고리즘 = new System.Windows.Forms.Label();
            this.lbl종목코드2 = new System.Windows.Forms.Label();
            this.btn삭제 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.txtTrader종목명 = new System.Windows.Forms.TextBox();
            this.btn등록 = new System.Windows.Forms.Button();
            this.lbl계좌번호 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            this.serviceController1 = new System.ServiceProcess.ServiceController();
            this.종목명 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.종목코드 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.알고리즘 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.현재가 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.보유수량 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.평균단가 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.손익금액 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.손익율 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.axKHOpenAPI)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupTrader등록해제.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTrader)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // axKHOpenAPI
            // 
            this.axKHOpenAPI.Enabled = true;
            this.axKHOpenAPI.Location = new System.Drawing.Point(228, 526);
            this.axKHOpenAPI.Name = "axKHOpenAPI";
            this.axKHOpenAPI.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axKHOpenAPI.OcxState")));
            this.axKHOpenAPI.Size = new System.Drawing.Size(100, 50);
            this.axKHOpenAPI.TabIndex = 11;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(6, 27);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(41, 12);
            this.Label1.TabIndex = 12;
            this.Label1.Text = "이름 : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(212, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 13;
            this.label2.Text = "아이디 : ";
            // 
            // lbl이름
            // 
            this.lbl이름.AutoSize = true;
            this.lbl이름.Location = new System.Drawing.Point(53, 27);
            this.lbl이름.Name = "lbl이름";
            this.lbl이름.Size = new System.Drawing.Size(0, 12);
            this.lbl이름.TabIndex = 14;
            // 
            // lbl아이디
            // 
            this.lbl아이디.AutoSize = true;
            this.lbl아이디.Location = new System.Drawing.Point(271, 27);
            this.lbl아이디.Name = "lbl아이디";
            this.lbl아이디.Size = new System.Drawing.Size(0, 12);
            this.lbl아이디.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(430, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 17;
            this.label3.Text = "계좌번호 : ";
            // 
            // cbo조건식
            // 
            this.cbo조건식.FormattingEnabled = true;
            this.cbo조건식.Location = new System.Drawing.Point(77, 20);
            this.cbo조건식.Name = "cbo조건식";
            this.cbo조건식.Size = new System.Drawing.Size(176, 20);
            this.cbo조건식.TabIndex = 23;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn_조건실시간중지);
            this.groupBox2.Controls.Add(this.btn조건실시간조회);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.btn_조건일반조회);
            this.groupBox2.Controls.Add(this.cbo조건식);
            this.groupBox2.Location = new System.Drawing.Point(390, 610);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(271, 90);
            this.groupBox2.TabIndex = 24;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "조건검색";
            // 
            // btn_조건실시간중지
            // 
            this.btn_조건실시간중지.Location = new System.Drawing.Point(178, 55);
            this.btn_조건실시간중지.Name = "btn_조건실시간중지";
            this.btn_조건실시간중지.Size = new System.Drawing.Size(75, 23);
            this.btn_조건실시간중지.TabIndex = 27;
            this.btn_조건실시간중지.Text = "실시간중지";
            this.btn_조건실시간중지.UseVisualStyleBackColor = true;
            this.btn_조건실시간중지.Click += new System.EventHandler(this.btn_조건실시간중지_Click);
            // 
            // btn조건실시간조회
            // 
            this.btn조건실시간조회.Location = new System.Drawing.Point(95, 55);
            this.btn조건실시간조회.Name = "btn조건실시간조회";
            this.btn조건실시간조회.Size = new System.Drawing.Size(77, 23);
            this.btn조건실시간조회.TabIndex = 26;
            this.btn조건실시간조회.Text = "실시간조회";
            this.btn조건실시간조회.UseVisualStyleBackColor = true;
            this.btn조건실시간조회.Click += new System.EventHandler(this.btn조건실시간조회_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 24);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 25;
            this.label12.Text = "조건식 : ";
            // 
            // btn_조건일반조회
            // 
            this.btn_조건일반조회.Location = new System.Drawing.Point(8, 55);
            this.btn_조건일반조회.Name = "btn_조건일반조회";
            this.btn_조건일반조회.Size = new System.Drawing.Size(71, 23);
            this.btn_조건일반조회.TabIndex = 25;
            this.btn_조건일반조회.Text = "일반조회";
            this.btn_조건일반조회.UseVisualStyleBackColor = true;
            this.btn_조건일반조회.Click += new System.EventHandler(this.btn_조건일반조회_Click);
            // 
            // groupTrader등록해제
            // 
            this.groupTrader등록해제.Controls.Add(this.dgvTrader);
            this.groupTrader등록해제.Controls.Add(this.cbo알고리즘);
            this.groupTrader등록해제.Controls.Add(this.lbl알고리즘);
            this.groupTrader등록해제.Controls.Add(this.lbl종목코드2);
            this.groupTrader등록해제.Controls.Add(this.btn삭제);
            this.groupTrader등록해제.Controls.Add(this.label13);
            this.groupTrader등록해제.Controls.Add(this.txtTrader종목명);
            this.groupTrader등록해제.Controls.Add(this.btn등록);
            this.groupTrader등록해제.Location = new System.Drawing.Point(14, 99);
            this.groupTrader등록해제.Name = "groupTrader등록해제";
            this.groupTrader등록해제.Size = new System.Drawing.Size(760, 491);
            this.groupTrader등록해제.TabIndex = 25;
            this.groupTrader등록해제.TabStop = false;
            this.groupTrader등록해제.Text = "Trader 등록 / 해제";
            // 
            // dgvTrader
            // 
            this.dgvTrader.AllowUserToAddRows = false;
            this.dgvTrader.AllowUserToDeleteRows = false;
            this.dgvTrader.AllowUserToResizeRows = false;
            this.dgvTrader.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgvTrader.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTrader.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvTrader.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTrader.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.종목명,
            this.종목코드,
            this.알고리즘,
            this.현재가,
            this.보유수량,
            this.평균단가,
            this.손익금액,
            this.손익율});
            this.dgvTrader.Location = new System.Drawing.Point(8, 95);
            this.dgvTrader.MultiSelect = false;
            this.dgvTrader.Name = "dgvTrader";
            this.dgvTrader.ReadOnly = true;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTrader.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvTrader.RowHeadersVisible = false;
            this.dgvTrader.RowTemplate.Height = 23;
            this.dgvTrader.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTrader.Size = new System.Drawing.Size(739, 390);
            this.dgvTrader.TabIndex = 34;
            // 
            // cbo알고리즘
            // 
            this.cbo알고리즘.FormattingEnabled = true;
            this.cbo알고리즘.Location = new System.Drawing.Point(571, 23);
            this.cbo알고리즘.Name = "cbo알고리즘";
            this.cbo알고리즘.Size = new System.Drawing.Size(176, 20);
            this.cbo알고리즘.TabIndex = 32;
            // 
            // lbl알고리즘
            // 
            this.lbl알고리즘.AutoSize = true;
            this.lbl알고리즘.Location = new System.Drawing.Point(491, 27);
            this.lbl알고리즘.Name = "lbl알고리즘";
            this.lbl알고리즘.Size = new System.Drawing.Size(61, 12);
            this.lbl알고리즘.TabIndex = 34;
            this.lbl알고리즘.Text = "알고리즘 :";
            // 
            // lbl종목코드2
            // 
            this.lbl종목코드2.AutoEllipsis = true;
            this.lbl종목코드2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.lbl종목코드2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl종목코드2.Location = new System.Drawing.Point(273, 23);
            this.lbl종목코드2.Margin = new System.Windows.Forms.Padding(3);
            this.lbl종목코드2.Name = "lbl종목코드2";
            this.lbl종목코드2.Size = new System.Drawing.Size(176, 21);
            this.lbl종목코드2.TabIndex = 33;
            this.lbl종목코드2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btn삭제
            // 
            this.btn삭제.Location = new System.Drawing.Point(653, 65);
            this.btn삭제.Name = "btn삭제";
            this.btn삭제.Size = new System.Drawing.Size(94, 24);
            this.btn삭제.TabIndex = 29;
            this.btn삭제.Text = "삭제";
            this.btn삭제.UseVisualStyleBackColor = true;
            this.btn삭제.Click += new System.EventHandler(this.Btn삭제_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 26);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(61, 12);
            this.label13.TabIndex = 26;
            this.label13.Text = "종 목 명 : ";
            // 
            // txtTrader종목명
            // 
            this.txtTrader종목명.Location = new System.Drawing.Point(77, 23);
            this.txtTrader종목명.Name = "txtTrader종목명";
            this.txtTrader종목명.Size = new System.Drawing.Size(176, 21);
            this.txtTrader종목명.TabIndex = 0;
            this.txtTrader종목명.TextChanged += new System.EventHandler(this.txtTrader종목명_TextChanged);
            this.txtTrader종목명.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTrader종목명_KeyDown);
            // 
            // btn등록
            // 
            this.btn등록.Location = new System.Drawing.Point(553, 65);
            this.btn등록.Name = "btn등록";
            this.btn등록.Size = new System.Drawing.Size(94, 24);
            this.btn등록.TabIndex = 28;
            this.btn등록.Text = "추가";
            this.btn등록.UseVisualStyleBackColor = true;
            this.btn등록.Click += new System.EventHandler(this.Btn추가_Click);
            // 
            // lbl계좌번호
            // 
            this.lbl계좌번호.AutoSize = true;
            this.lbl계좌번호.Location = new System.Drawing.Point(491, 27);
            this.lbl계좌번호.Name = "lbl계좌번호";
            this.lbl계좌번호.Size = new System.Drawing.Size(0, 12);
            this.lbl계좌번호.TabIndex = 27;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Label1);
            this.groupBox1.Controls.Add(this.lbl계좌번호);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.lbl아이디);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lbl이름);
            this.groupBox1.Location = new System.Drawing.Point(14, 17);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(760, 59);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "계좌 정보";
            // 
            // 종목명
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.종목명.DefaultCellStyle = dataGridViewCellStyle2;
            this.종목명.Frozen = true;
            this.종목명.HeaderText = "종목명";
            this.종목명.Name = "종목명";
            this.종목명.ReadOnly = true;
            this.종목명.Width = 97;
            // 
            // 종목코드
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.종목코드.DefaultCellStyle = dataGridViewCellStyle3;
            this.종목코드.Frozen = true;
            this.종목코드.HeaderText = "종목코드";
            this.종목코드.Name = "종목코드";
            this.종목코드.ReadOnly = true;
            this.종목코드.Width = 83;
            // 
            // 알고리즘
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.NullValue = null;
            this.알고리즘.DefaultCellStyle = dataGridViewCellStyle4;
            this.알고리즘.Frozen = true;
            this.알고리즘.HeaderText = "알고리즘";
            this.알고리즘.Name = "알고리즘";
            this.알고리즘.ReadOnly = true;
            this.알고리즘.Width = 122;
            // 
            // 현재가
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N0";
            dataGridViewCellStyle5.NullValue = null;
            this.현재가.DefaultCellStyle = dataGridViewCellStyle5;
            this.현재가.Frozen = true;
            this.현재가.HeaderText = "현재가";
            this.현재가.Name = "현재가";
            this.현재가.ReadOnly = true;
            this.현재가.Width = 87;
            // 
            // 보유수량
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N0";
            dataGridViewCellStyle6.NullValue = null;
            this.보유수량.DefaultCellStyle = dataGridViewCellStyle6;
            this.보유수량.Frozen = true;
            this.보유수량.HeaderText = "보유수량";
            this.보유수량.Name = "보유수량";
            this.보유수량.ReadOnly = true;
            this.보유수량.Width = 86;
            // 
            // 평균단가
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N0";
            dataGridViewCellStyle7.NullValue = null;
            this.평균단가.DefaultCellStyle = dataGridViewCellStyle7;
            this.평균단가.Frozen = true;
            this.평균단가.HeaderText = "평균단가";
            this.평균단가.Name = "평균단가";
            this.평균단가.ReadOnly = true;
            this.평균단가.Width = 87;
            // 
            // 손익금액
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N0";
            dataGridViewCellStyle8.NullValue = null;
            this.손익금액.DefaultCellStyle = dataGridViewCellStyle8;
            this.손익금액.Frozen = true;
            this.손익금액.HeaderText = "손익금액";
            this.손익금액.Name = "손익금액";
            this.손익금액.ReadOnly = true;
            this.손익금액.Width = 87;
            // 
            // 손익율
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Red;
            dataGridViewCellStyle9.Format = "N2";
            dataGridViewCellStyle9.NullValue = null;
            this.손익율.DefaultCellStyle = dataGridViewCellStyle9;
            this.손익율.Frozen = true;
            this.손익율.HeaderText = "손익율";
            this.손익율.Name = "손익율";
            this.손익율.ReadOnly = true;
            this.손익율.Width = 87;
            // 
            // TradingCenter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(787, 602);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupTrader등록해제);
            this.Controls.Add(this.axKHOpenAPI);
            this.Name = "TradingCenter";
            this.Text = "Trading Center";
            ((System.ComponentModel.ISupportInitialize)(this.axKHOpenAPI)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupTrader등록해제.ResumeLayout(false);
            this.groupTrader등록해제.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTrader)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private AxKHOpenAPILib.AxKHOpenAPI axKHOpenAPI; 
        private System.Windows.Forms.Label Label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl이름;
        private System.Windows.Forms.Label lbl아이디;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.ComboBox cbo조건식;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn_조건실시간중지;
        private System.Windows.Forms.Button btn조건실시간조회;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btn_조건일반조회;
        private System.Windows.Forms.GroupBox groupTrader등록해제;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtTrader종목명;
        private System.Windows.Forms.Button btn등록;
        private System.Windows.Forms.Button btn삭제;
        private System.Windows.Forms.Label lbl계좌번호;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbl종목코드2;
        private System.Windows.Forms.ComboBox cbo알고리즘;
        private System.Windows.Forms.Label lbl알고리즘;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
        private System.ServiceProcess.ServiceController serviceController1;
        private System.Windows.Forms.DataGridView dgvTrader;
        private System.Windows.Forms.DataGridViewTextBoxColumn 종목명;
        private System.Windows.Forms.DataGridViewTextBoxColumn 종목코드;
        private System.Windows.Forms.DataGridViewTextBoxColumn 알고리즘;
        private System.Windows.Forms.DataGridViewTextBoxColumn 현재가;
        private System.Windows.Forms.DataGridViewTextBoxColumn 보유수량;
        private System.Windows.Forms.DataGridViewTextBoxColumn 평균단가;
        private System.Windows.Forms.DataGridViewTextBoxColumn 손익금액;
        private System.Windows.Forms.DataGridViewTextBoxColumn 손익율;
    }
}

