﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenStockTrader
{
    public class Trader
    {
        string str계좌번호;
        private string _str종목코드;
        private string _str종목명;
        private Algorithm _algorithm;

        public Algorithm algorithm { get { return _algorithm; } }

        public Trader(string str계좌번호, string str종목명, string str종목코드, Algorithm algorithm)
        {
            this.str계좌번호 = str계좌번호;
            this._str종목명 = str종목명;
            this._str종목코드 = str종목코드;
            this._algorithm = algorithm;
            FileLogger.WriteLine("00", "Trader", "Constructor",
                "계좌번호:" + str계좌번호 +
                " | 종목명:" + str종목명 + 
                " | 종목코드:" + str종목코드);
        }
        
        public string str종목코드 { get { return _str종목코드; } }
        public string str종목명 { get { return _str종목명; } }

        public void OnReceiveRealData(Hashtable eventItem)
        {
            string eventID = (string)eventItem["eventID"];
            string str종목명 = (string)eventItem["종목명"];
            string str종목코드 = (string)eventItem["종목코드"];
            string str실시간타입 = (string)eventItem["실시간타입"];

            FileLogger.WriteLine(eventID, "Trader", "OnReceiveRealData",
                "종목명:" + str종목명 + " | 종목코드:" + str종목코드 + " | 실시간타입:" + str실시간타입);

            Algorithm.Order order = algorithm.ProvideRealData(str실시간타입, eventItem);
            FileLogger.WriteLineEx(eventID, "OnReceiveRealData - 매수매도:" + order.s매수매도 + " | 주문가격:" + order.n주문가격 + " | 주문수량:" + order.n주문수량);
            거래요청(eventID, order);
        }
        
        public void OnReceiveChejanData(Hashtable eventItem)
        {
            string eventID = (string)eventItem["eventID"];
            string str종목명 = (string)eventItem["종목명"];

            FileLogger.WriteLine(eventID, "Trader", "OnReceiveChejanData",
                "종목명:" + str종목명);

            algorithm.ResultOfOrder(eventItem);
        }

        private void 거래요청(string eventID, Algorithm.Order order)
        {
            FileLogger.WriteLine(eventID, "Trader", "거래요청",
                "매수매도:" + order.s매수매도 +
                " | 계좌번호:" + str계좌번호 +
                " | 종목코드:" + _str종목코드 +
                " | 주문가격:" + order.n주문가격 +
                " | 주문수량:" + order.n주문수량);

            int n주문수량 = order.n주문수량;
            int n주문가격 = order.n주문가격;

            switch (order.s매수매도)
            {
                case "매수":
                    {
                        int lRet = API.SendOrder("매수주문", "44444", str계좌번호,
                                                                        1/*매수*/, _str종목코드, n주문수량, n주문가격, "00", "");
                        if (lRet == 0)
                        {
                            FileLogger.WriteLineEx(eventID, "매수 주문 전송(" + str종목명 + ")을 성공 하였습니다.");
                        }
                        else
                        {
                            FileLogger.WriteLineEx(eventID, "매수 주문 전송(" + str종목명 + ")을 실패 하였습니다.  에러코드:" + lRet);
                        }
                    }
                    break;

                case "매도":
                    {
                        int lRet = API.SendOrder("매도주문", "88888", str계좌번호,
                                                                        2/*매도*/, _str종목코드, n주문수량, n주문가격, "00", "");
                        if (lRet == 0)
                        {
                            FileLogger.WriteLineEx(eventID, "매도 주문 전송(" + str종목명 + ")을 성공 하였습니다.");
                        }
                        else
                        {
                            FileLogger.WriteLineEx(eventID, "매도 주문 전송(" + str종목명 + ")을 실패 하였습니다.  에러코드:" + lRet);
                        }
                    }
                    break;

                default:
                    break;
            }
        }
    }
}
