﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenStockTrader
{
    class Order
    {
        public enum OrderType { BUY=1, SELL=2};
        public enum OrderPriceType { UNKNOWN=0, BELOW=1, AROUND=2};
        public enum OrderStage { GOT_SMS=0, WAIT_PRICE_RESPONSE, }

        public OrderType type { get; set; }
        public string item { get; set; }
        public int price { get; set; }
        public OrderPriceType priceType { get; set; }
        public float volume { get; set; }
        public OrderStage stage { get; set; }

        public Order()
        {
            stage = OrderStage.GOT_SMS;
        }

        public override string ToString()
        {
            return "type: " + type + " item: " + item + " price: "+price +" priceType: "+priceType+" volume: "+volume;
        }
    }
}
