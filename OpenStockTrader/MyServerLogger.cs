﻿using Grapevine.Interfaces.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenStockTrader
{
    class MyServerLogger : IGrapevineLogger
    {
        public LogLevel Level => throw new NotImplementedException();

        public void Debug(object obj)
        {
            FileLogger.WriteLine("", "Grapevine", "", obj.ToString());
        }

        public void Debug(string message)
        {
            FileLogger.WriteLine("", "Grapevine", "", message);
        }

        public void Debug(string message, Exception ex)
        {
            FileLogger.WriteLine("", "Grapevine", "", message + " exception: "+ex.Message);
        }

        public void Error(object obj)
        {
            FileLogger.WriteLine("", "Grapevine", "", obj.ToString());
        }

        public void Error(string message)
        {
            FileLogger.WriteLine("", "Grapevine", "", message);
        }

        public void Error(string message, Exception ex)
        {
            FileLogger.WriteLine("", "Grapevine", "", message + " exception: " + ex.Message);
        }

        public void Fatal(object obj)
        {
            throw new NotImplementedException();
        }

        public void Fatal(string message)
        {
            FileLogger.WriteLine("", "Grapevine", "", message);
        }

        public void Fatal(string message, Exception ex)
        {
            FileLogger.WriteLine("", "Grapevine", "", message + " exception: " + ex.Message);
        }

        public void Info(object obj)
        {
            FileLogger.WriteLine("", "Grapevine", "", obj.ToString());
        }

        public void Info(string message)
        {
            FileLogger.WriteLine("", "Grapevine", "", message);
        }

        public void Info(string message, Exception ex)
        {
            FileLogger.WriteLine("", "Grapevine", "", message + " exception: " + ex.Message);
        }

        public void Log(LogEvent evt)
        {
            FileLogger.WriteLine("", "Grapevine", "", evt.Message);
        }

        public void Trace(object obj)
        {
            FileLogger.WriteLine("", "Grapevine", "", obj.ToString());
        }

        public void Trace(string message)
        {
            FileLogger.WriteLine("", "Grapevine", "", message);
        }

        public void Trace(string message, Exception ex)
        {
            FileLogger.WriteLine("", "Grapevine", "", message + " exception: " + ex.Message);
        }

        public void Warn(object obj)
        {
            FileLogger.WriteLine("", "Grapevine", "", obj.ToString());
        }

        public void Warn(string message)
        {
            FileLogger.WriteLine("", "Grapevine", "", message);
        }

        public void Warn(string message, Exception ex)
        {
            FileLogger.WriteLine("", "Grapevine", "", message + " exception: " + ex.Message);
        }
    }
}
