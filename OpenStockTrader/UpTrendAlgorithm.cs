﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace OpenStockTrader
{
    class UpTrendAlgorithm:Algorithm
    {

        public int status; // 1: 주문가능, 0:주문체결결과대기

        public int n금일수익매도가능주식수;
        public int n금일손실매도가능주식수;

        public SortedList<int, int> stockList;

        public int n현재가;
        public float f등락율;

        public float f수익매도기준수익율;
        public float f손실매도기준손실율;
        public float f매수기준전일대비하락율;
        public int n전일대비하락시매수주식수;
        public bool b전일대비하락시매수완료;

        public UpTrendAlgorithm()
        {
            n금일수익매도가능주식수 = 2;
            n금일손실매도가능주식수 = 2;
            n전일대비하락시매수주식수 = 2;

            f수익매도기준수익율 = 5F;
            f손실매도기준손실율 = -7F;
            f매수기준전일대비하락율 = -1F;

            status = 1;
            b전일대비하락시매수완료 = false;
            ReadStockList();
            SaveStockList();  // 읽은 내용을 다시 파일로 기록
        }

        public override Order ProvideRealData(string str실시간타입, Hashtable eventItem)
        {
            string eventID = (string)eventItem["eventID"];
            FileLogger.WriteLine(eventID, GetFileName(), "ProvideRealData",
                "실시간타입:" + str실시간타입);

            Algorithm.Order order = new Algorithm.Order();  // Order 구조체 초기화

            if (status == 0) return order;   // 주문결과대기 중에는 주문 하지 않음

            switch (str실시간타입)
            {
                case "주식체결":
                    n현재가 = Math.Abs(int.Parse((string)eventItem["현재가"]));
                    f등락율 = float.Parse((string)eventItem["등락율"], CultureInfo.InvariantCulture);
                    FileLogger.WriteLineEx(eventID, "현재가:" + n현재가.ToString());
                    FileLogger.WriteLineEx(eventID, "등락율:" + f등락율.ToString());
                    break;

                default:
                    FileLogger.WriteLineEx(eventID, "Do nothing. Return");
                    return order ;
            }

            // 매도 -----------------------------------------------------------------------------------------
            order = 매도(eventID, order, f수익매도기준수익율, f손실매도기준손실율);

            // 매수 -----------------------------------------------------------------------------------------
            order = 전일대비하락시매수(eventID, order);

            return order;
        }

        public override void ResultOfOrder(Hashtable eventItem)
        {
            string eventID = (string)eventItem["eventID"];
            string str체결구분 = (string)eventItem["체결구분"];
            string str체결수량 = (string)eventItem["체결수량"];
            string str체결가격 = (string)eventItem["체결가격"];

            FileLogger.WriteLine(eventID, GetFileName(), "ResultOfOrder",
                "체결구분:" + str체결구분 + " | " + "체결수량:" + str체결수량 + " | " + "체결가격:" + str체결가격);

            if (status == 0)
            {
                FileLogger.WriteLineEx(eventID, "SaveStockList");
                status = 1;
                SaveStockList();
            }
        }

        public string GetFileName()
        {
            return this.ToString().Split('.')[1];
        }

        public void ReadStockList()
        {
            stockList = new SortedList<int, int>();

            string fileName = GetFileName() + ".db";
            FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Read);
            StreamReader st = new StreamReader(fs, Encoding.UTF8);

            st.BaseStream.Seek(0, SeekOrigin.Begin);

            while (st.Peek() > -1)
            {
                string[] item = st.ReadLine().Split(' ');
                stockList.Add(int.Parse(item[0].Trim()), int.Parse(item[1].Trim()));
            }

            st.Close();
            fs.Close();
        }

        public void SaveStockList()
        {
            string fileName = GetFileName() + ".db";
            FileStream fs = new FileStream(fileName, FileMode.Create, FileAccess.Write);
            StreamWriter st = new StreamWriter(fs, Encoding.UTF8);

            foreach (KeyValuePair<int, int> kv in stockList)
            {
                st.WriteLine(kv.Key + " " + kv.Value);
            }

            st.Close();
            fs.Close();
        }

        public  Order 매도(string eventID, Order order, float f수익상한율, float f수익하한율)
        {
            float f수익금액 = n현재가 *( f수익상한율 *( 0.01F));
            float f손실금액 = n현재가 * (f수익하한율 * (-0.01F));
            int n수익매도기준가 = n현재가 - (int)f수익금액;
            int n손실매도기준가 = n현재가 + (int)f손실금액;
            int n매도주식수 = 0;

            FileLogger.WriteLine(eventID, GetFileName(), "매도",
                "수익상한율:" + f수익상한율.ToString() + "%" +
                " | 수익하한율:" + f수익하한율.ToString() + "%" +
                " | 수익금액:" + f수익금액.ToString() +
                " | 손실금액:" + f손실금액.ToString() +
                " | 수익매도기준가:" + n수익매도기준가.ToString() +
                " | 손실매도기준가: " + n손실매도기준가.ToString());

            // 수익매도
            for (int index = 0; index < stockList.Count(); index++)
            {
                int key = stockList.Keys[index];
                int value = stockList.Values[index];

                if (key <= n수익매도기준가)
                {
                    FileLogger.WriteLineEx(eventID, "수익매도 Index:" + index + " | 보유주식가격:" + key + " | 주식수:" + value);

                    int n매도가능주식수 = 수익매도가능주식수확인(value);
                    int n매도불가능주식수 = value - n매도가능주식수;

                    n매도주식수 += n매도가능주식수;
                    stockList.Remove(key);

                    if (n매도불가능주식수 != 0)
                    {
                        stockList.Add(key, n매도불가능주식수);
                        break;  // 더이상 매도가 불가능하므로 순회를 멈춘다.
                    }
                }
                else
                {
                    FileLogger.WriteLineEx(eventID, "수익매도순회멈춤");
                    break;   // 수익매도기준가보다 크면 매도를 하지 않으므로 순회를 멈춘다.
                }
            }

            // 손실매도
            for (int index = stockList.Count() - 1; index >= 0; index--)
            {
                int key = stockList.Keys[index];
                int value = stockList.Values[index];

                if (key >= n손실매도기준가)
                {
                    FileLogger.WriteLineEx(eventID, "손실매도 Index:" + index + " | 보유주식가격:" + key + " | 주식수:" + value);

                    int n매도가능주식수 = 손실매도가능주식수확인(value);
                    int n매도불가능주식수 = value - n매도가능주식수;

                    n매도주식수 += n매도가능주식수;
                    stockList.Remove(key);

                    if (n매도불가능주식수 != 0)
                    {
                        stockList.Add(key, n매도불가능주식수);
                        break;  // 더이상 매도가 불가능하므로 순회를 멈춘다.
                    }
                }
                else
                {
                    FileLogger.WriteLineEx(eventID, "손실매도순회멈춤");
                    break;   // 손실매도기준가보다 작으면 매도를 하지 않으므로 순회를 멈춘다.
                }
            }

            if (n매도주식수 > 0)
            {
                status = 0;
                order.s매수매도 = "매도";
                order.n주문가격 = n현재가;
                order.n주문수량 = n매도주식수;
                FileLogger.WriteLineEx(eventID, "주문 매수매도:" + order.s매수매도 + " | 주문가격:" + order.n주문가격 + " | 주문수량:" + order.n주문수량);
            }

            return order;
        }

        public Order 전일대비하락시매수(string eventID, Order order)
        {
            if (DateTime.Now.Hour == 15)
            {
                FileLogger.WriteLine(eventID, GetFileName(), "전일대비하락시매수",
                    "f등락율 : " + f등락율.ToString() + "%" +
                    " | f매수기준전일하락율 : " + f매수기준전일대비하락율.ToString() + "%" +
                    " | b전일대비하락시매수완료 : " + b전일대비하락시매수완료.ToString() +
                    " | n금일손실매도가능주식수:" + n금일손실매도가능주식수.ToString());
            }

            if ((DateTime.Now.Hour == 15) 
                && (f등락율 < f매수기준전일대비하락율) 
                && !b전일대비하락시매수완료             // 매수는 한번만
                && (n금일손실매도가능주식수 > 0))             // 손실매도가 발생한 날은 매수하지 않음
            {
                FileLogger.WriteLineEx(eventID, "15시 이후 등락율 < " + f매수기준전일대비하락율.ToString());
                status = 0;
                order.s매수매도 = "매수";
                order.n주문가격 = n현재가;
                order.n주문수량 = n전일대비하락시매수주식수;
                b전일대비하락시매수완료 = true;
                if(stockList.ContainsKey(n현재가))
                {
                    int n기존보유주식수 = stockList[n현재가];
                    stockList.Remove(n현재가);
                    stockList.Add(n현재가, n전일대비하락시매수주식수 + n기존보유주식수);
                }
                else
                {
                    stockList.Add(n현재가, n전일대비하락시매수주식수);
                }
            }

            return order;
        }

        public int 수익매도가능주식수확인(int 매도주식수)
        {
            int n매도가능주식수 = 0;
            if (n금일수익매도가능주식수 <= 매도주식수)
            {
                n매도가능주식수 = n금일수익매도가능주식수;
                n금일수익매도가능주식수 = 0;
            }
            else
            {
                n매도가능주식수 = 매도주식수;
                n금일수익매도가능주식수 = n금일수익매도가능주식수 - 매도주식수;
            }
            return n매도가능주식수;
        }

        public int 손실매도가능주식수확인(int 매도주식수)
        {
            int n매도가능주식수 = 0;
            if (n금일손실매도가능주식수 <= 매도주식수)
            {
                n매도가능주식수 = n금일손실매도가능주식수;
                n금일손실매도가능주식수 = 0;
            }
            else
            {
                n매도가능주식수 = 매도주식수;
                n금일손실매도가능주식수 = n금일손실매도가능주식수 - 매도주식수;
            }
            return n매도가능주식수;
        }
    }
}
