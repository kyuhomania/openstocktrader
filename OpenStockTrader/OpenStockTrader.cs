﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace OpenStockTrader
{
    public class OpenStockTrader
    {

        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main()
        {
            System.Diagnostics.Debug.WriteLine("Hello, OpenStockTrader");
            DateTime thisDay = DateTime.Today;
            FileLogger.SetLogFile("Logs_" + thisDay.ToString("yyyy-MM-dd") + ".txt");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new TradingCenter());
        }
    }
}
