﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OpenStockTrader
{
    class OrderDupChecker
    {
        private readonly static int MAX_CAPACITY = 10;
        List<string> buyList;
        List<string> sellList;

        public OrderDupChecker()
        {
            buyList = new List<string>();
            sellList = new List<string>(); ;
        }

        public Boolean isDuplicate(Order order)
        {
            if (order.type == Order.OrderType.BUY)
            {
                foreach (string item in buyList) {
                    if (item.Equals(order.item))
                    {
                        return true;
                    }
                }

                buyList.Add(order.item);
                if (buyList.Count > MAX_CAPACITY)
                {
                    buyList.RemoveAt(0);
                }
                return false;
            }
            else if (order.type == Order.OrderType.SELL)
            {
                foreach (string item in sellList)
                {
                    if (item.Equals(order.item))
                    {
                        return true;
                    }
                }

                sellList.Add(order.item);
                if (sellList.Count > MAX_CAPACITY)
                {
                    sellList.RemoveAt(0);
                }
                return false;
            }
            return false;
        }
    }
}
