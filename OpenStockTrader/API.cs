﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenStockTrader
{
    public class API
    {
        private static AxKHOpenAPILib.AxKHOpenAPI _axKHOpenAPI;
        private static Hashtable ht종목명리스트;

        public static void SetAPIControl(AxKHOpenAPILib.AxKHOpenAPI axKHOpenAPI)
        {
            _axKHOpenAPI = axKHOpenAPI;

            string str종목코드리스트 = _axKHOpenAPI.GetCodeListByMarket("0");   // 0 - 장내
            str종목코드리스트 += _axKHOpenAPI.GetCodeListByMarket("10");   // 10 - 코스닥
            str종목코드리스트 += _axKHOpenAPI.GetCodeListByMarket("8");   // 8 - 장내, 8 -ETF
            string[] arr종목코드 = str종목코드리스트.Split(';');
            ht종목명리스트 = new Hashtable();

            for (int i = 0; i < arr종목코드.Length; i++)
            {
                string str종목코드 = arr종목코드[i].Trim();
                byte[] b종목코드 = Encoding.UTF8.GetBytes(str종목코드);

                if (b종목코드.Length <= 0) continue;
                if (b종목코드[0] == 'A') str종목코드 = str종목코드.Substring(1);

                if (ht종목명리스트.ContainsValue(str종목코드) == false)
                {
                    ht종목명리스트.Add(axKHOpenAPI.GetMasterCodeName(str종목코드), str종목코드);
                }
            }
        }

        public static string Get종목코드(string str종목명)
        {
            return (string)ht종목명리스트[str종목명];
        }

        public static string Get종목명(string str종목코드)
        {
            string _str종목코드 = str종목코드.Trim();
            byte[] b종목코드 = Encoding.UTF8.GetBytes(_str종목코드.Trim());
            if (b종목코드[0] == 'A') _str종목코드 = _str종목코드.Substring(1);
            return _axKHOpenAPI.GetMasterCodeName(_str종목코드.Trim());
        }

        // ID:3
        // Tran을 서버로 송신한다.
        // sRQName – 사용자구분 명
        // sTrCode - Tran명 입력
        // nPrevNext - 0:조회, 2:연속
        // sScreenNo - 4자리의 화면번호
        // Ex) openApi.CommRqData( “RQ_1”, “OPT00001”, 0, “0101”);
        // 반환값
        // OP_ERR_SISE_OVERFLOW – 과도한 시세조회로 인한 통신불가
        // OP_ERR_RQ_STRUCT_FAIL – 입력 구조체 생성 실패
        // OP_ERR_RQ_STRING_FAIL – 요청전문 작성 실패
        // OP_ERR_NONE – 정상처리
        public static int CommRqData(
            string sRQName, 
            string sTrCode, 
            int nPrevNext, 
            string sScreenNo)
        {
            return _axKHOpenAPI.CommRqData(sRQName, sTrCode, nPrevNext, sScreenNo);
        }

        // ID:4
        // 로그인한 사용자 정보를 반환한다.
        // sTag - 사용자 정보 구분 TAG값
        // sTag에 들어 갈 수 있는 값은 아래와 같음
        //“ACCOUNT_CNT” – 전체 계좌 개수를 반환한다.
        // "ACCNO" – 전체 계좌를 반환한다.계좌별 구분은 ‘;’이다.
        //“USER_ID” - 사용자 ID를 반환한다.
        //“USER_NAME” – 사용자명을 반환한다.
        //“KEY_BSECGB” – 키보드보안 해지여부. 0:정상, 1:해지
        //“FIREW_SECGB” – 방화벽 설정 여부. 0:미설정, 1:설정, 2:해지
        // Ex) openApi.GetLoginInfo(“ACCOUNT_CNT”);
        // 반환값 데이터
        public static string GetLoginInfo(
            string sTag)
        {
            return _axKHOpenAPI.GetLoginInfo(sTag);
        }

        // ID:5
        // 주식 주문을 서버로 전송한다.
        // sRQName - 사용자 구분 요청 명
        // sScreenNo - 화면번호[4]
        // sAccNo - 계좌번호[10]
        // nOrderType - 주문유형(1:신규매수, 2:신규매도, 3:매수취소, 4:매도취소, 5:매수정정, 6:매도정정)
        // sCode, - 주식종목코드
        // 반환값 0:성공, 나머지 실패
        public static int SendOrder(
            string sRQName,
            string sScreenNo,
            string sAccNo,
            int nOrderType,
            string sCode,
            int nQty,
            int nPrice,
            string sHogaGb,
            string sOrgOrderNo)
        {
            return _axKHOpenAPI.SendOrder(sRQName, sScreenNo, sAccNo, nOrderType, sCode, nQty, nPrice, sHogaGb, sOrgOrderNo);
        }

        // ID:7
        // Tran 입력 값을 서버통신 전에 입력한다.
        // sID – 아이템명
        // sValue – 입력 값
        // Ex) openApi.SetInputValue(“종목코드”, “000660”);
        // openApi.SetInputValue(“계좌번호”, “5015123401”);
        // 반환값 없음
        public static void SetInputValue(
            string sID,
            string sValue)
        {
            _axKHOpenAPI.SetInputValue(sID, sValue);
        }

        // ID:9
        // Tran 데이터, 실시간 데이터, 체결잔고 데이터를 반환한다.
        // ○1 Tran 데이터
        // sJongmokCode : Tran명
        // sRealType : 사용안함
        // sFieldName : 레코드명
        // nIndex : 반복인덱스
        // sInnerFieldName: 아이템명
        // ○2 실시간 데이터
        // sJongmokCode : Key Code
        // sRealType : Real Type
        // sFieldName : Item Index
        // nIndex : 사용안함
        // sInnerFieldName:사용안함
        // ○3 체결 데이터
        // sJongmokCode : 체결구분
        // sRealType : “-1”
        // sFieldName : 사용안함
        // nIndex : ItemIndex
        // sInnerFieldName :사용안함
        // 반환값 - 요청 데이터
        // ex) TR정보 요청 - openApi.CommGetData(“OPT00001”, “”, “주식기본정보”, 0, “현재가”);
        // ex) 실시간정보 요청 - openApi.CommGetData(“000660”, “A”, 0);
        // ex) 체결정보 요청 - openApi.CommGetData(“000660”, “-1”, 1);
        public static string CommGetData(
            string sJongmokCode,
            string sRealType,
            string sFieldName,
            int nIndex,
            string sInnerFieldName)
        {
            return _axKHOpenAPI.CommGetData(sJongmokCode, sRealType, sFieldName, nIndex, sInnerFieldName);
        }


        // ID:10
        // 리얼데이터 요청을 제거한다.
        // sScnNo – 화면번호[4]
        // 화면을 종료할 때 반드시 dl 함수를 호출해야 한다
        // 반환값 - void
        public static void DisconnectRealData(
            string sScnNo)
        {
            _axKHOpenAPI.DisconnectRealData(sScnNo);
        }

        // ID:11
        // 수신 받은 Tran 데이터의 레코드 개수 반환
        // sTrCode – Tran 명
        // sRecordName – 레코드 명
        // 반환값 - 레코드 개수
        // ex) GetRepeatCnt(“OPT00001”, “주식기본정보”);
        public static int GetRepeatCnt(
            string sTrCode,
            string sRecordName)
        {
            return _axKHOpenAPI.GetRepeatCnt(sTrCode, sRecordName);
        }

        // ID:15
        // 로그인 윈도우 실행
        // 반환값 0:미연결, 0:연결
        public static int GetConnectState()
        {
            return _axKHOpenAPI.GetConnectState();
        }

        // ID:25
        // 실시간데이터를 반환한다.
        // strRealType – 실시간 구분
        // nFid – 실시간 아이템
        // 반환값 수신 데이터
        // Ex) 현재가출력 - openApi.GetCommRealData(“주식시세”, 10);
        // 참고)실시간 현재가는 주식시세, 주식체결 등 다른 실시간타입(RealType)으로도 수신가능
        public static string GetCommRealData(
            string strRealType, 
            int nFid)
        {
            return _axKHOpenAPI.GetCommRealData(strRealType, nFid);
        }

        // ID:49
        // 종목별 실시간 모니터링 등록
        // strScreenNo – 실시간 등록할 화면 번호
        // strCodeList – 실시간 등록할 종목코드(복수종목가능 – “종목1; 종목2;종목3;….”)
        // strFidList – 실시간 등록할 FID(“FID1; FID2;FID3;…..”)
        // strRealType – “0”, “1” 타입
        // strRealType이 “0” 으로 하면 같은화면에서 다른종목 코드로 실시간 등록을 하게 되면 마지막에 
        // 사용한 종목코드만 실시간 등록이 되고 기존에 있던 종목은 실시간이 자동 해지됨.
        // “1”로 하면 같은화면에서 다른 종목들을 추가하게 되면 기존에 등록한 종목도 함께 실시간 시세를 받을 수 있음.
        // 꼭 같은 화면이여야 하고 최초 실시간 등록은 “0”으로 하고 이후부터 “1”로 등록해야함.
        // 반환값 0:성공, 나머지 실패
        public static int SetRealReg(
            string strScreenNo,
            string strCodeList,
            string FidList,
            string strRealType)
        {
            return _axKHOpenAPI.SetRealReg(strScreenNo, strCodeList, FidList, strRealType);
        }


        // ID:50
        // 종목별 실시간 모니터링 해제
        // strScrNo – 실시간 해제할 화면 번호
        // strDelCode – 실시간 해제할 종목
        // 반환값 void
        public static void SetRealRemove (
            string strScrNo,
            string strDelCode)
        {
            _axKHOpenAPI.SetRealRemove(strScrNo, strDelCode);
        }

        // ID:51
        // 서버에 저장된 사용자 조건식을 조회해서 임시로 파일에 저장.
        // System 폴더에 아이디_NewSaveIndex.dat파일로 저장된다. Ocx가 종료되면 삭제시킨다.
        // 조건검색 사용시 이함수를 최소 한번은 호출해야 조건검색을 할 수 있다.
        // 영웅문에서 사용자 조건을 수정 및 추가하였을 경우에도 최신의 사용자 조건을 받고 싶으면 다시 조회해야한다.
        // 반환값 ?
        public static int GetConditionLoad()
        {
            return _axKHOpenAPI.GetConditionLoad();
        }

        // ID:53
        // 조건검색 종목조회TR송신한다.
        // string strScrNo : 화면번호
        // string strConditionName : 조건명
        // int nIndex : 조건명인덱스
        // int nSearch : 조회구분(0:일반조회, 1:실시간조회, 2:연속조회)
        // 단순 조건식에 맞는 종목을 조회하기 위해서는 조회구분을 0으로 하고,
        // 실시간 조건검색을 하기 위해서는 조회구분을 1로 한다.
        // OnReceiveTrCondition으로 결과값이 온다.
        // 연속조회가 필요한 경우에는 응답받는 곳에서 연속조회 여부에 따라 연속조회를 송신하면된다.
        // 반환값 1:성공 0:실패
        public static int SendCondition(
            string strScrNo,
            string strConditionName,
            int nIndex,
            int nSearch)
        {
            return _axKHOpenAPI.SendCondition(strScrNo, strConditionName, nIndex, nSearch);
        }


        public static void SendConditionStop(
            string strScrNo, 
            string strConditionName, 
            int nIndex)
        {
            _axKHOpenAPI.SendConditionStop(strScrNo, strConditionName, nIndex);
        }
    }
}
