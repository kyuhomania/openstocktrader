﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenStockTrader
{
    public class KiwoomEventHandler
    {
        private TradingCenter tradingCenter;
        private AxKHOpenAPILib.AxKHOpenAPI axKHOpenAPI;
        private Queue<Hashtable> eventQueue;
        private Queue<Hashtable> megaStockQueue;

        public KiwoomEventHandler(TradingCenter tradingCenter, AxKHOpenAPILib.AxKHOpenAPI axKHOpenAPI, Queue<Hashtable> eventQueue, Queue<Hashtable> megaStockQueue)
        {
            this.tradingCenter = tradingCenter;
            this.axKHOpenAPI = axKHOpenAPI;
            this.eventQueue = eventQueue;
            this.megaStockQueue = megaStockQueue;
        }

        private static int eventID = 10;
        private string getEventID()
        {
            eventID++;
            if (eventID > 99) eventID = 10;
            return eventID.ToString();
        }

        // EventOnReceiveTrData
        // 서버통신 후 데이터를 받은 시점을 알려준다.
        // string sScrNo – 화면번호
        // string sRQName – 사용자구분 명. CommRqData의 sRQName과 매핑되는 이름이다.
        // string sTrCode – Tran 명. CommRqData의 sTrCode과 매핑되는 이름이다.
        // string sRecordName – Record 명
        // string sPreNext – 연속조회 유무. 0: 연속(추가조회)데이터 없음, 1:연속(추가조회) 데이터 있음
        public void OnReceiveTrData(object sender, AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveTrDataEvent e)
        {
            string eventID = getEventID();
            FileLogger.WriteLine(eventID, "KiwoomEventHandler", "OnReceiveTrData", 
                "화면번호:" + e.sScrNo + " | RQName:" + e.sRQName + " | TrCode:" + e.sTrCode + " | RecordName:" + e.sRecordName + " | RevNext: " + e.sPrevNext);

            if (e.sRQName == "MegaStock")
            {
                switch (e.sTrCode)
                {
                    case "OPT10001":
                        // OPT1001 : 주식기본정보
                        {
                            Hashtable eventItem = new Hashtable();

                            int repeatCnt = axKHOpenAPI.GetRepeatCnt(e.sTrCode, e.sRQName);

                            string 종목명 = axKHOpenAPI.GetCommData(e.sTrCode, e.sRQName, 0, "종목명").Trim();
                            string 현재가 = axKHOpenAPI.GetCommData(e.sTrCode, e.sRQName, 0, "현재가").Trim();

                            eventItem.Add("eventID", eventID);
                            eventItem.Add("종목명", 종목명);
                            eventItem.Add("현재가", 현재가);
                            eventItem.Add("TrCode", e.sTrCode);

                            megaStockQueue.Enqueue(eventItem);
                        }
                        break;
                    case "OPW00004":
                        {
                            int repeatCnt = axKHOpenAPI.GetRepeatCnt(e.sTrCode, e.sRQName);

                            for (int i = 0; i < repeatCnt; i++)
                            {
                                string 종목명 = axKHOpenAPI.GetCommData(e.sTrCode, e.sRQName, i, "종목명").Trim();
                                string 보유수량 = axKHOpenAPI.GetCommData(e.sTrCode, e.sRQName, i, "보유수량").Trim();
                                string 평균단가 = axKHOpenAPI.GetCommData(e.sTrCode, e.sRQName, i, "평균단가").Trim();

                                Hashtable eventItem = new Hashtable();
                                eventItem.Add("eventID", eventID);
                                eventItem.Add("종목명", 종목명);
                                eventItem.Add("보유수량", 보유수량);
                                eventItem.Add("평균단가", 평균단가);
                                eventItem.Add("TrCode", e.sTrCode);

                                megaStockQueue.Enqueue(eventItem);
                            }
                        }
                        break;
                }
            }
            else
            {
                switch (e.sTrCode)
                {
                    case "KOA_NORMAL_BUY_KP_ORD":
                        {
                            Hashtable eventItem = new Hashtable();
                            eventItem.Add("eventID", eventID);
                            eventItem.Add("이벤트", "OnReceiveTrData");
                            eventItem.Add("요청이름", e.sRQName);
                            eventItem.Add("TrCode", e.sTrCode);

                            string str원주문번호 = axKHOpenAPI.GetCommData(e.sTrCode, "", 0, "").Trim();
                            eventItem.Add("원주문번호", str원주문번호);

                            eventQueue.Enqueue(eventItem);
                        }
                        break;

                    case "OPT10001":
                        // OPT1001 : 주식기본정보
                        {
                            Hashtable eventItem = new Hashtable();
                            eventItem.Add("eventID", eventID);
                            eventItem.Add("이벤트", "OnReceiveTrData");
                            eventItem.Add("요청이름", e.sRQName);
                            eventItem.Add("TrCode", e.sTrCode);

                            int repeatCnt = axKHOpenAPI.GetRepeatCnt(e.sTrCode, e.sRQName);
                            FileLogger.WriteLineEx(eventID, "TrCode: OPT10001, " + "repeatCnt: " + repeatCnt);
                            FileLogger.WriteLineEx(eventID, "요청이름:" + e.sRQName +
                                " | TrCode:" + e.sTrCode);

                            eventQueue.Enqueue(eventItem);
                        }
                        break;

                    case "OPT10081":
                        // OPT10081 : 주식일봉차트조회
                        {
                            int repeatCnt = axKHOpenAPI.GetRepeatCnt(e.sTrCode, e.sRQName);
                            for (int i = 0; i < repeatCnt; i++)
                            {
                                Hashtable eventItem = new Hashtable();
                                eventItem.Add("eventID", eventID);
                                eventItem.Add("이벤트", "OnReceiveTrData");
                                eventItem.Add("요청이름", e.sRQName);
                                eventItem.Add("TrCode", e.sTrCode);

                                FileLogger.WriteLineEx(eventID, "요청이름:" + e.sRQName +
                                    " | TrCode:" + e.sTrCode);

                                eventQueue.Enqueue(eventItem);
                            }
                        }
                        break;

                    case "OPW00004":
                        {
                            int repeatCnt = axKHOpenAPI.GetRepeatCnt(e.sTrCode, e.sRQName);
                            FileLogger.WriteLineEx(eventID, "repeatCnt:" + repeatCnt);
                            for (int i = 0; i < repeatCnt; i++)
                            {
                                Hashtable eventItem = new Hashtable();
                                eventItem.Add("eventID", eventID);
                                eventItem.Add("이벤트", "OnReceiveTrData");
                                eventItem.Add("요청이름", e.sRQName);
                                eventItem.Add("TrCode", e.sTrCode);

                                string 요청이름 = e.sRQName.Trim();
                                string 종목코드 = Verify종목코드(axKHOpenAPI.GetCommData(e.sTrCode, e.sRQName, i, "종목코드").Trim());
                                string 종목명 = axKHOpenAPI.GetCommData(e.sTrCode, e.sRQName, i, "종목명").Trim();
                                string 보유수량 = axKHOpenAPI.GetCommData(e.sTrCode, e.sRQName, i, "보유수량").Trim();
                                string 평균단가 = axKHOpenAPI.GetCommData(e.sTrCode, e.sRQName, i, "평균단가").Trim();
                                string 현재가 = axKHOpenAPI.GetCommData(e.sTrCode, e.sRQName, i, "현재가").Trim();
                                string 평가금액 = axKHOpenAPI.GetCommData(e.sTrCode, e.sRQName, i, "평가금액").Trim();
                                string 손익금액 = axKHOpenAPI.GetCommData(e.sTrCode, e.sRQName, i, "손익금액").Trim();
                                string 손익율 = axKHOpenAPI.GetCommData(e.sTrCode, e.sRQName, i, "손익율").Trim();
                                string 대출일 = axKHOpenAPI.GetCommData(e.sTrCode, e.sRQName, i, "대출일").Trim();
                                string 매입금액 = axKHOpenAPI.GetCommData(e.sTrCode, e.sRQName, i, "매입금액").Trim();
                                string 결제잔고 = axKHOpenAPI.GetCommData(e.sTrCode, e.sRQName, i, "결제잔고").Trim();
                                string 전일매수수량 = axKHOpenAPI.GetCommData(e.sTrCode, e.sRQName, i, "전일매수수량").Trim();
                                string 전일매도수량 = axKHOpenAPI.GetCommData(e.sTrCode, e.sRQName, i, "전일매도수량").Trim();
                                string 금일매수수량 = axKHOpenAPI.GetCommData(e.sTrCode, e.sRQName, i, "금일매수수량").Trim();
                                string 금일매도수량 = axKHOpenAPI.GetCommData(e.sTrCode, e.sRQName, i, "금일매도수량").Trim();

                                FileLogger.WriteLineEx(eventID, "요청이름:" + e.sRQName +
                                    " | 종목코드:" + 종목코드 +
                                    " | 종목명:" + 종목명 +
                                    " | 보유수량:" + 보유수량 +
                                    " | 평균단가:" + 평균단가 +
                                    " | 현재가" + 현재가 +
                                    " | 평가금액:" + 평가금액 +
                                    " | 손익금액:" + 손익금액 +
                                    " | 손익율:" + 손익율 +
                                    " | 대출일:" + 대출일 +
                                    " | 매입금액:" + 매입금액 +
                                    " | 결제잔고:" + 결제잔고 +
                                    " | 전일매수수량:" + 전일매수수량 +
                                    " | 전일매도수량:" + 전일매도수량 +
                                    " | 금일매수수량:" + 금일매수수량 +
                                    " | 금일매도수량:" + 금일매도수량);

                                eventItem.Add("종목코드", 종목코드);
                                eventItem.Add("종목명", 종목명);
                                eventItem.Add("보유수량", 보유수량);
                                eventItem.Add("평균단가", 평균단가);
                                eventItem.Add("현재가", 현재가);
                                eventItem.Add("평가금액", 평가금액);
                                eventItem.Add("손익금액", 손익금액);
                                eventItem.Add("손익율", 손익율);
                                eventItem.Add("대출일", 대출일);
                                eventItem.Add("매입금액", 매입금액);
                                eventItem.Add("결제잔고", 결제잔고);
                                eventItem.Add("전일매수수량", 전일매수수량);
                                eventItem.Add("전일매도수량", 전일매도수량);
                                eventItem.Add("금일매수수량", 금일매수수량);
                                eventItem.Add("금일매도수량", 금일매도수량);

                                eventQueue.Enqueue(eventItem);
                            }
                        }
                        break;
                }
            }
        }

        // EventOnReceiveRealData
        // 실시간데이터를 받은 시점을 알려준다.
        // string sRealKey – 종목코드
        // string sRealType – 실시간타입
        // string sRealData – 실시간데이터전문
        public void OnReceiveRealData(object sender, AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveRealDataEvent e)
        {
            string eventID = getEventID();
            //FileLogger.WriteLine(eventID, "KiwoomEventHandler", "OnReceiveRealData",
            //    "종목코드:" + e.sRealKey + " | 실시간타입:" + e.sRealType + " | 실시간데이터전문:" + e.sRealData);

            Hashtable eventItem = new Hashtable();
            eventItem.Add("eventID", eventID);
            eventItem.Add("이벤트", "OnReceiveRealData");
            eventItem.Add("실시간타입", e.sRealType);
            eventItem.Add("종목코드", Verify종목코드(e.sRealKey));
            eventItem.Add("종목명", axKHOpenAPI.GetMasterCodeName(e.sRealKey));

            switch (e.sRealType)
            {
                case "주식시세":
                    break;

                case "주식체결":
                    {
                        string 체결시간 = axKHOpenAPI.GetCommRealData(e.sRealKey, 20).Trim();
                        string 현재가 = axKHOpenAPI.GetCommRealData(e.sRealKey, 10).Trim();
                        string 전일대비 = axKHOpenAPI.GetCommRealData(e.sRealKey, 11).Trim();
                        string 등락율 = Verify등락율(axKHOpenAPI.GetCommRealData(e.sRealType, 12).Trim());
                        string 최우선_매도호가 = axKHOpenAPI.GetCommRealData(e.sRealKey, 27).Trim();
                        string 최우선_매수호가 = axKHOpenAPI.GetCommRealData(e.sRealKey, 28).Trim();
                        string 거래량 = axKHOpenAPI.GetCommRealData(e.sRealKey, 15).Trim();
                        string 누적거래량 = axKHOpenAPI.GetCommRealData(e.sRealKey, 13).Trim();
                        string 누적거래대금 = axKHOpenAPI.GetCommRealData(e.sRealKey, 14).Trim();
                        string 시가 = axKHOpenAPI.GetCommRealData(e.sRealKey, 16).Trim();
                        string 고가 = axKHOpenAPI.GetCommRealData(e.sRealKey, 17).Trim();
                        string 저가 = axKHOpenAPI.GetCommRealData(e.sRealKey, 18).Trim();
                        string 전일대비기호 = axKHOpenAPI.GetCommRealData(e.sRealKey, 25).Trim();
                        string 전일거래량대비_계약 = axKHOpenAPI.GetCommRealData(e.sRealKey, 26).Trim();
                        string 거래대금증감 = axKHOpenAPI.GetCommRealData(e.sRealKey, 29).Trim();
                        string 전일거래량대비_비율 = axKHOpenAPI.GetCommRealData(e.sRealKey, 30).Trim();
                        string 거래회전율 = axKHOpenAPI.GetCommRealData(e.sRealKey, 31).Trim();
                        string 거래비용 = axKHOpenAPI.GetCommRealData(e.sRealKey, 32).Trim();
                        string 체결강도 = axKHOpenAPI.GetCommRealData(e.sRealKey, 228).Trim();
                        string 시가총액_억 = axKHOpenAPI.GetCommRealData(e.sRealKey, 311).Trim();
                        string 장구분 = axKHOpenAPI.GetCommRealData(e.sRealKey, 290).Trim();
                        string KO접근도 = axKHOpenAPI.GetCommRealData(e.sRealKey, 691).Trim();
                        string 상한가발생시간 = axKHOpenAPI.GetCommRealData(e.sRealKey, 567).Trim();
                        string 하한가발생시간 = axKHOpenAPI.GetCommRealData(e.sRealKey, 568).Trim();
                        eventItem.Add("체결시간", 체결시간);
                        eventItem.Add("현재가", 현재가);
                        eventItem.Add("전일대비", 전일대비);
                        eventItem.Add("등락율", 등락율);
                        eventItem.Add("최우선_매도호가", 최우선_매도호가);
                        eventItem.Add("최우선_매수호가", 최우선_매수호가);
                        eventItem.Add("거래량", 거래량);
                        eventItem.Add("누적거래량", 누적거래량);
                        eventItem.Add("누적거래대금", 누적거래대금);
                        eventItem.Add("시가", 시가);
                        eventItem.Add("고가", 고가);
                        eventItem.Add("저가", 저가);
                        eventItem.Add("전일대비기호", 전일대비기호);
                        eventItem.Add("전일거래량대비_계약", 전일거래량대비_계약);
                        eventItem.Add("거래대금증감", 거래대금증감);
                        eventItem.Add("전일거래량대비_비율", 전일거래량대비_비율);
                        eventItem.Add("거래회전율", 거래회전율);
                        eventItem.Add("거래비용", 거래비용);
                        eventItem.Add("체결강도", 체결강도);
                        eventItem.Add("시가총액_억", 시가총액_억);
                        eventItem.Add("장구분", 장구분);
                        eventItem.Add("KO접근도", KO접근도);
                        eventItem.Add("상한가발생시간", 상한가발생시간);
                        eventItem.Add("하한가발생시간", 하한가발생시간);

                        /*
                        FileLogger.WriteLineEx(eventID, "실시간타입:주식체결" +
                            " | 종목명:" + axKHOpenAPI.GetMasterCodeName(e.sRealKey) +
                            " | 체결시간:" + 체결시간 +
                            " | 현재가:" + 현재가 +
                            " | 전일대비:" + 전일대비 +
                            " | 등락율:" + 등락율 +
                            " | 최우선_매도호가:" + 최우선_매도호가 +
                            " | 최우선_매수호가:" + 최우선_매수호가 +
                            " | 거래량:" + 거래량 +
                            " | 누적거래량:" + 누적거래량 +
                            " | 누적거래대금:" + 누적거래대금 +
                            " | 시가:" + 시가 +
                            " | 고가:" + 고가 +
                            " | 저가:" + 저가 +
                            " | 전일대비기호:" + 전일대비기호 +
                            " | 전일거래량대비_계약:" + 전일거래량대비_계약 +
                            " | 거래대금증감:" + 거래대금증감 +
                            " | 전일거래량대비_비율:" + 전일거래량대비_비율 +
                            " | 거래회전율:" + 거래회전율 +
                            " | 거래비용:" + 거래비용 +
                            " | 체결강도:" + 체결강도 +
                            " | 시가총액_억:" + 시가총액_억 +
                            " | 장구분:" + 장구분 +
                            " | KO접근도:" + KO접근도 +
                            " | 상한가발생시간:" + 상한가발생시간 +
                            " | 하한가발생시간:" + 하한가발생시간);
                            */
                    }
                    break;

                case "주식예상체결":
                    break;

                case "주식거래원":
                    string str종목코드 = Verify종목코드(axKHOpenAPI.GetCommRealData(e.sRealKey, 9001).Trim());  // 종목코드
                    string str거래소구분 = axKHOpenAPI.GetCommRealData(e.sRealKey, 337).Trim();  // 거래소구분
                    eventItem.Add("종목코드", str종목코드);
                    eventItem.Add("거래소구분", str거래소구분);

                    FileLogger.WriteLineEx(eventID, "실시간타입:주식거래원" +
                        " | 종목코드:" + str종목코드 +
                        " | 거래소구분:" + str거래소구분);

                    break;

                case "주문체결":
                    break;

                case "잔고":
                    break;

                case "순간체결량":
                    break;

            }

            eventQueue.Enqueue(eventItem);
        }

        // EventOnReceiveMsg
        // 서버통신 후 메시지를 받은 시점을 알려준다.
        // string sScrNo – 화면번호
        // string sRQName – 사용자구분 명
        // string sTrCode – Tran 명
        // string sMsg – 서버메시지
        public void OnReceiveMsg(object sender, AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveMsgEvent e)
        {
            string eventID = getEventID();
            FileLogger.WriteLine(eventID, "KiwoomEventHandler", "OnReceiveMsg",
               "화면번호:" + e.sScrNo + " | 요청이름:" +  e.sRQName + " | TrCode:" + e.sTrCode + " | 메시지:" + e.sMsg);

            Hashtable eventItem = new Hashtable();
            eventItem.Add("eventID", eventID);
            eventItem.Add("이벤트", "OnReceiveMsg");
            eventItem.Add("요청이름", e.sRQName);

            eventQueue.Enqueue(eventItem);
        }

        // EventOnReceiveChejanData
        // 체결데이터를 받은 시점을 알려준다.
        // string sGubun – 체결구분. 0:주문체결통보, 1:잔고통보, 3:특이신호
        // int nItemCnt - 아이템갯수
        // string sFIdList – 데이터리스트. 데이터 구분은 ‘;’ 이다.
        public void OnReceiveChejanData(object sender, AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveChejanDataEvent e)
        {
            string eventID = getEventID();
            FileLogger.WriteLine(eventID, "KiwoomEventHandler", "OnReceiveChejanData",
                "체결구분:" + e.sGubun + " | 아이템갯수:" + e.nItemCnt + " | 데이터리스트:" + e.sFIdList);

            //string[] arr데이터리스트 = e.sFIdList.Split(';');

            for (int iCnt = 0; iCnt < e.nItemCnt; iCnt++)
            {
            }

            Hashtable eventItem = new Hashtable();
            eventItem.Add("eventID", eventID);
            eventItem.Add("이벤트", "OnReceiveChejanData");

            if (e.sGubun == "0")
            {
                string str주문체결시간 = axKHOpenAPI.GetChejanData(908).Trim();
                string str종목명 = axKHOpenAPI.GetChejanData(302).Trim();
                string str주문수량 = axKHOpenAPI.GetChejanData(900).Trim();
                string str주문가격 = axKHOpenAPI.GetChejanData(901).Trim();
                string str체결수량 = axKHOpenAPI.GetChejanData(911).Trim();
                string str체결가격 = axKHOpenAPI.GetChejanData(910).Trim();

                eventItem.Add("체결구분", "주문체결통보");
                eventItem.Add("주문체결시간", str주문체결시간);
                eventItem.Add("종목명", str종목명);
                eventItem.Add("주문수량", str주문수량);
                eventItem.Add("주문가격", str주문가격);
                eventItem.Add("체결수량", str체결수량);
                eventItem.Add("체결가격", str체결가격);

                FileLogger.WriteLineEx(eventID, "체결구분:주문체결통보" +
                    " | 주문체결시간:" + str주문체결시간 +
                    " | 종목명:" + str종목명 +
                    " | 주문수량:" + str주문수량 +
                    " | 주문가격:" + str주문가격 +
                    " | 체결수량:" + str체결수량 +
                    " | 체결가격:" + str체결가격);
            }
            else if (e.sGubun == "1")
            {
                string str종목명 = axKHOpenAPI.GetChejanData(302);
                FileLogger.WriteLineEx(eventID, "체결구분:잔고통보" +
                        " | 종목명:" + str종목명);
            }
            else if (e.sGubun == "3")
            {
                FileLogger.WriteLineEx(eventID, "체결구분:특이신호");
            }

            eventQueue.Enqueue(eventItem);
        }

        // EventOnEventConnect
        // 서버 접속 관련 이벤트
        // int nErrCode : 에러 코드
        public void OnEventConnect(object sender, AxKHOpenAPILib._DKHOpenAPIEvents_OnEventConnectEvent e)
        {
            string eventID = getEventID();
            FileLogger.WriteLine(eventID, "KiwoomEventHandler", "OnEventConnect",
                "에러코드:" + e.nErrCode);

            KiwoomCode.Error.IsError(e.nErrCode);
            FileLogger.WriteLineEx(eventID, "로그인 처리결과:" + KiwoomCode.Error.GetErrorMessage());

            tradingCenter.서버연결이벤트();
        }

        // EventOnReceiveCondition
        // 조건검색 실시간 편입,이탈 종목을 받을 시점을 알려준다.
        // strConditionName에 해당하는 종목이 실시간으로 들어옴.
        // strType으로 편입된 종목인지 이탈된 종목인지 구분한다.
        // string sTrCode : 종목코드
        // string strType : 편입(“I”), 이탈(“D”)
        // string strConditionName : 조건명
        // string strConditionIndex : 조건명 인덱스
        public void OnReceiveRealCondition(object sender, AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveRealConditionEvent e)
        {
            string eventID = getEventID();
            FileLogger.WriteLine(eventID, "KiwoomEventHandler", "OnReceiveRealCondition",
                "종목코드:" + e.sTrCode + " | 편입이탈:" + e.strType + " | 조건명:" + e.strConditionName + " | 조건명인덱스:" + e.strConditionIndex);

            Hashtable eventItem = new Hashtable();
            eventItem.Add("eventID", eventID);
            eventItem.Add("이벤트", "OnReceiveRealCondition");

            eventQueue.Enqueue(eventItem);
        }

        // EventOnReceiveTrCondition
        // 조건검색 조회응답으로 종목리스트를 구분자(“;”)로 붙어서 받는 시점.
        // string sScrNo : 화면번호
        // string strCodeList : 종목코드리스트(“;”로 구분)
        // string strConditionName : 조건명
        // int nIndex : 조건명 인덱스
        // int nNext : 연속조회(2:연속조회, 0:연속조회없음)
        public void OnReceiveTrCondition(object sender, AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveTrConditionEvent e)
        {
            string eventID = getEventID();
            FileLogger.WriteLine(eventID, "KiwoomEventHandler", "OnReceiveTrCondition",
                "화면번호:" + e.sScrNo + " | 종목코드리스트:" + e.strCodeList + " | 조건명:" + e.strConditionName + " | 조건명인덱스:" + e.nIndex.ToString() + " | 연속조회:" + e.nNext.ToString());

            Hashtable eventItem = new Hashtable();
            eventItem.Add("eventID", eventID);
            eventItem.Add("이벤트", "OnReceiveTrCondition");

            eventQueue.Enqueue(eventItem);
        }

        // EventOnReceiveConditionVer
        // 로컬에 사용자 조건식 저장 성공 여부를 확인하는 시점
        // int lRet : 사용자 조건식 저장 성공여부 (1: 성공, 나머지 실패)
        public void OnReceiveConditionVer(object sender, AxKHOpenAPILib._DKHOpenAPIEvents_OnReceiveConditionVerEvent e)
        {
            string eventID = getEventID();
            FileLogger.WriteLine(eventID, "KiwoomEventHandler", "OnReceiveConditionVer",
                "사용자 조건식 저장 성공여부:" + e.lRet);

            Hashtable eventItem = new Hashtable();
            eventItem.Add("eventID", eventID);
            eventItem.Add("이벤트", "OnReceiveConditionVer");

            if (e.lRet == 0)
            {
                return;
            }

            // 조건명 리스트를 구분(“;”)하여 받아온다. Ex) 인덱스1^조건명1;인덱스2^조건명2;인덱스3^조건명3;…
            // 반환값 조건명 리스트(인덱스^조건명)
            string condition = axKHOpenAPI.GetConditionNameList();
            eventItem.Add("조건식", condition);

            // 분리된 문자 배열 저장
            string[] spConList = condition.Split(';');

            // ComboBox 출력
            for (int i = 0; i < spConList.Length; i++)
            {
                if (spConList[i].Trim().Length >= 2)
                {
                    string[] spCon = spConList[i].Split('^');
                    int nIndex = Int32.Parse(spCon[0]);
                    string strConditionName = spCon[1];
                    tradingCenter.cbo조건식.Items.Add(strConditionName);
                }
            }

            tradingCenter.cbo조건식.SelectedIndex = 0;

            eventQueue.Enqueue(eventItem);
        }

        private string Verify종목코드(string str종목코드)
        {
            byte[] b종목코드 = Encoding.UTF8.GetBytes(str종목코드.Trim());
            if (b종목코드[0] == 'A') return str종목코드.Substring(1);
            else return str종목코드;
        }

        private string Verify등락율(string str등락율)
        {
            byte[] b등락율 = Encoding.UTF8.GetBytes(str등락율.Trim());
            if (b등락율[0] == '+') return str등락율.Substring(1);
            else return str등락율;
        }
    }
}
