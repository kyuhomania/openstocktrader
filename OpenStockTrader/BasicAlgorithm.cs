﻿using System;
using System.Collections;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenStockTrader
{
    public class BasicAlgorithm : Algorithm
    {
        private int n최대매수가능주식수 = 2;
        private int n최대매도가능주식수 = 2;
        private int n회당거래주식수 = 1;

        string str등락율 = "0";
        string str누적거래량 = "0";

        public override Order ProvideRealData(string str실시간타입, Hashtable eventItem)
        {
            string eventID = (string)eventItem["eventID"];
            FileLogger.WriteLine(eventID, "BasicAlgorithm", "ProvideRealData",
                "실시간타입:" + str실시간타입);

            Algorithm.Order order = new Algorithm.Order();  // Order 구조체 초기화

            if (str현재가 == "0") return order;

            switch (str실시간타입)
            {
                case "주식체결":
                    str현재가 = (string)eventItem["현재가"];
                    str등락율 = (string)eventItem["등락율"];
                    str누적거래량 = (string)eventItem["누적거래량"];
                    string str체결강도 = (string)eventItem["체결강도"];
                    string str체결시간 = (string)eventItem["체결시간"];
                    FileLogger.WriteLineEx(eventID, "현재가:" + str현재가);
                    FileLogger.WriteLineEx(eventID, "등락율:" + str등락율);
                    FileLogger.WriteLineEx(eventID, "누적거래량:" + str누적거래량);
                    FileLogger.WriteLineEx(eventID, "체결강도:" + str체결강도);
                    FileLogger.WriteLineEx(eventID, "체결시간:" + str체결시간);
                    break;

                case "주식거래원":
                    string str거래소구분 = (string)eventItem["거래소구분"];
                    FileLogger.WriteLineEx(eventID, "거래소구분:" + str거래소구분);
                    break;
            }

            bool b매수가능 = false;
            bool b매도가능 = false;
            b매수가능 = (n최대매도가능주식수 > n회당거래주식수) ? b매수가능 = true : b매수가능 = false;
            b매도가능 = (n최대매수가능주식수 > n회당거래주식수) ? b매도가능 = true : b매도가능 = false;

            float f등락율 = float.Parse(str등락율, CultureInfo.InvariantCulture);
            FileLogger.WriteLineEx(eventID, "f등락율:" + f등락율);

            if ((f등락율 > 3) && b매도가능)
            {
                FileLogger.WriteLineEx(eventID, "등락율 > 3");
                order.s매수매도 = "매도";
                order.n주문가격 = Math.Abs(int.Parse(str현재가));
                order.n주문수량 = n회당거래주식수;
                n최대매도가능주식수--;
            }
            else if ((f등락율 < -3) && b매수가능)
            {
                FileLogger.WriteLineEx(eventID, "등락율 < -3");
                order.s매수매도 = "매수";
                order.n주문가격 = Math.Abs(int.Parse(str현재가));
                order.n주문수량 = n회당거래주식수;
                n최대매수가능주식수--;
            }

            return order;
        }

        public override void ResultOfOrder(Hashtable eventItem)
        {
        }
    }
}
