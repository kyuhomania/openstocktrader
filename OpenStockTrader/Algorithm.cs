﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenStockTrader
{
    public abstract class Algorithm
    {
        abstract public Order ProvideRealData(string str실시간타입, Hashtable eventItem);
        abstract public void ResultOfOrder(Hashtable eventItem);

        private string _str종목명;
        private string _str종목코드;
        private string _str현재가;
        private string _str보유수량;
        private string _str평균단가;

        public string str종목명 { get { return _str종목명; } set { _str종목명 = value; } }
        public string str종목코드 { get { return _str종목코드; } set { _str종목코드 = value; } }
        public string str현재가 { get { return _str현재가; } set { _str현재가 = value; } }
        public string str보유수량 { get { return _str보유수량; } set { _str보유수량 = value; } }
        public string str평균단가 { get { return _str평균단가; } set { _str평균단가 = value; } }

        public struct Order  // 주문
        {
            private string _s매수매도; // "매수" or "매도". 이 외의 값은 주문이 들어가지 않는다.
            private int _n주문수량;
            private int _n주문가격;
            
            public Order(string s매수매도, int n주문가격, int n주문수량)
            {
                _s매수매도 = s매수매도;
                _n주문가격 = n주문가격;
                _n주문수량 = n주문수량;
            }

            public string s매수매도 { get { return _s매수매도; } set { _s매수매도 = value; } }
            public int n주문가격 { get { return _n주문가격; } set { _n주문가격 = value; } }
            public int n주문수량 { get { return _n주문수량; } set { _n주문수량 = value; } }
        }
    }
}
