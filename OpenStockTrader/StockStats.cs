﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenStockTrader
{
    class StockStats
    {
        public string 종목명 { get; set; }
        public string 알고리즘 { get; set; }
        public string 현재가 { get; set; }
        public string 보유수량 { get; set; }
        public string 평균단가 { get; set; }
        public string 수익금액 { get; set; }
        public string 수익율 { get; set; }
    }
}
